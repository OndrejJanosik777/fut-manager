import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import moment from 'moment';
import TopMenu from '../../components/top-menu';
import editPicture from './assets/pencil-square.svg';
import deletePicture from './assets/trash3.svg';
import NavBar from '../../components/nav-bar';
import ModalSearchPlayers from '../../modal-components/search-players';
import Modal_Delete from '../../modal-components/delete';
import './index.scss';

/*
    this page will allow logged user to manage his team... 
    data needed:
    - user data
    - players data
*/

const ManageTeam = (props) => {
    const params = useParams();
    const axios = require('axios');
    const navigate = useNavigate();

    const getBaseUrl = () => {
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // dev code
            return 'http://127.0.0.1:8000';
        } else {
            // production code
            return 'https://fut-v3.herokuapp.com';
        }
    }

    const [baseUrl, setBaseUrl] = useState(getBaseUrl());
    const [team, setTeam] = useState({ 
        name: 'no-team',
        players: [],
    });
    const [futTeamName, setFutTeamName] = useState('');
    const [displayModalSearchPlayers, set_displayModalSearchPlayers] = useState(false);
    const [displayModal_Delete, set_displayModal_Delete] = useState(false);
    const [activeItemId, setActiveItemId] = useState(0);

    const [NP_first_name, setNP_first_name] = useState('');
    const [NP_middle_name, setNP_middle_name] = useState('');
    const [NP_last_name, setNP_last_name] = useState('');
    const [NP_position, setNP_position] = useState('');
    const [NP_status, setNP_status] = useState('');

    const [token, setToken] = useState("Bearer " + localStorage.getItem('FUT_user_token_access'));
    const [token_refresh, setTokenRefresh] = useState(localStorage.getItem('FUT_user_token_refresh'));
    const [user, setUser] = useState({
        username: "",
        first_name: "",
        last_name: "",
        date_joined: "",
        last_login: "",
    });

    useEffect(() => {
        console.log('page manage-team loaded...')
        checkToken();
        getUserData();
        fetchTeamData();
    }, []);

    const checkToken = () => {
        console.log('function refresh token ...');

        axios({
            method: 'post',
            url: baseUrl + '/api/token/refresh/',
            data: {
                refresh: token_refresh
            }
        })
            .then((response => {
                console.log('token refreshed succesfully');

                localStorage.setItem('FUT_user_token_access', response.data.access);

                setToken("Bearer " + response.data.access);
            }))
            .catch((error) => {
                console.log(error);

                alert('function checktoken: not valid credentials...');

                navigate('/');
            })
    }

    const getUserData = () => {
        console.log('function getUserData  ...');

        axios({
            method: 'get',
            url: baseUrl + '/api/fifa/me',
            headers: {
                "Authorization": token
            }
        })
            .then((response => {
                console.log('user info got succesfully succesfully');

                console.log("user: ", response.data);

                setUser(response.data);
            }))
            .catch((error) => {
                console.log(error);

                alert('not valid credentials...');

                navigate('/');
            })
    }

    const modifyPlayerInTeam = (player) => {
        let index = team.players.findIndex(element => element.id === player.id);

        let newTeam = {...team};

        if (index !== -1) {
            console.log('removing player from team: ', player.id);

            newTeam.players.splice(index, 1);
        }
        else {
            console.log('adding player to the team: ', player.id);

            newTeam.players = [...newTeam.players, player];
        }

        // updating team from component state
        setTeam(newTeam);
        
        // updating team in backend
        console.log('updating team in backend: ', newTeam);

        let playersIds = [];

        newTeam.players.map((item) => {
            playersIds.push(item.id);
        })

        axios({
            method: 'put',
            url: baseUrl + '/api/fifa/teams/' + newTeam.id + '/',
            headers: { 'Authorization': token },
            data: {
                id: newTeam.id,
                name: newTeam.name,
                players: playersIds,
                // players: [14112],
                user: newTeam.user,
            }
        }).then((response) => {
            console.log("team updated");
            console.log("response.data: ", response.data);

            set_displayModal_Delete(false);

        }).catch((error) => {
            console.log("team not updated: ", error);

            set_displayModal_Delete(false);
        })
    }

    const createFutTeam = () => {
        // console.log('function createFutTeam...');

        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'post',
            url: baseUrl + '/api/fut/team/',
            headers: { 'Authorization': token },
            data: {
                name: futTeamName,
            }
        }).then((response) => {
            console.log("team create: ", response.data);

            // TODO: refresh token 

            fetchTeamData(token);
        }).catch((error) => {
            console.log(error);
        })
    }

    const deletePlayer = (playerId) => {
        console.log('deleting player with id: ', playerId);

        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'delete',
            url: baseUrl + '/api/fut/delete-player/' + playerId + '/',
            headers: { 'Authorization': token },
        }).then((response) => {
            // console.log('players of loggedUser FUT Team: ', response.data);
            // fetchPlayers();
        }).catch((error) => {
            console.log(error);
        })
    }

    const deleteItem = (id) => {
        setActiveItemId(id);

        set_displayModal_Delete(true);
    }

    const deleteItemFromBackend = () =>{
        modifyPlayerInTeam(activeItemId);
    }

    const fetchTeamData = () => {
        const queryParams = new URLSearchParams(window.location.search);
        const teamId = queryParams.get('team');
        console.log('fetching: ',  teamId);

        axios({
            method: 'get',
            url: baseUrl + `/api/fifa/teams/${teamId}/`,
            headers: { 'Authorization': token },
        }).then((response) => {
            console.log("team: ", response.data);

            setTeam(response.data);
        }).catch((error) => {
            console.log(error);
        })
    }

    const showState = () => {
        console.log('showing state of component');
        const queryParams = new URLSearchParams(window.location.search);
        const teamId = queryParams.get('team');
        const clubId = queryParams.get('club');

        console.log('team id is: ', teamId);
        console.log('club id is: ', clubId);
        console.log('team: ', team);
    }

    return (<div className='manage-your-team'> 
        { displayModal_Delete ?
            <Modal_Delete text={'Are you sure to delete player from this team?'} actionYes={deleteItemFromBackend} actionNo={() => set_displayModal_Delete(!displayModal_Delete)} /> :
            <div></div>
        }
        { displayModalSearchPlayers ?
            <ModalSearchPlayers 
                close={() => set_displayModalSearchPlayers(!displayModalSearchPlayers)} 
                players={team.players} 
                modifyPlayerInTeam={modifyPlayerInTeam}
            /> :
            <div></div>
        }
        <NavBar user={user} />
        <h1 className='h_1' onClick={showState}>{team.name}</h1>
        <main className='main'>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col" className='w-40'>Player</th>
                        <th scope="col" className='w-20'>S</th>
                        <th scope="col" className='w-20'>G</th>
                        <th scope="col" className='w-10'>A</th>
                        <th scope="col" className='w-10'></th>
                    </tr>
                </thead>
                <tbody>
                    {team.players.map((item) => {
                        return  <tr key={Math.random() * 100000}>
                                    <td>{item.commonName}</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td><img src={deletePicture} alt={""} onClick={() => deleteItem(item)} /></td>
                                </tr>
                    })}
                </tbody>
            </table>
        </main>
        <div className='buttons-container' >
            <input type='button' value='Add Players' className='button_1' onClick={() => set_displayModalSearchPlayers(!displayModalSearchPlayers)} />
            <input type='button' value='Teams overview' className='button_1' onClick={() => navigate('/manage-teams/')} />
        </div>
    </div>);
}

export default ManageTeam;