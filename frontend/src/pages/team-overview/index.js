import React, { Component, useState, useEffect } from 'react';
import { Link, useParams, useNavigate } from 'react-router-dom';
import TopMenu from '../../components/top-menu';
import '../index.scss';

const TeamOverview = () => {
    const params = useParams();
    const axios = require('axios');
    const navigate = useNavigate();

    const getBaseUrl = () => {
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // dev code
            return 'http://127.0.0.1:8000';
        } else {
            // production code
            return 'https://fut-v2.herokuapp.com';
        }
    }

    const [baseUrl, setBaseUrl] = useState(getBaseUrl());
    const [loggedUser, setLoggedUser] = useState({ username: '' });
    const [pageloaded, setPageLoaded] = useState(0);
    const [players, setPlayers] = useState([]);
    const [team, setTeam] = useState([{ name: '' }]);
    const [totalTeamGoals, setTotalTeamGoals] = useState(0);
    const [filterActivePlayers, setFilterActivePlayers] = useState(false);
    const [filterForwardPlayer, setFilterForwardPlayer] = useState(false);
    const [filterMiddlePlayer, setFilterMiddlePlayer] = useState(false);
    const [filterBackPlayer, setFilterBackPlayer] = useState(false);

    const checkToken = () => {
        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'get',
            url: baseUrl + '/api/fut/user/',
            headers: { 'Authorization': token },
        }).then((response) => {
            console.log("logged user data: ", response.data);

            // TODO: refresh token 

            setLoggedUser(response.data);
        }).catch((error) => {
            console.log(error);

            navigate('/');
        })
    }

    const fetchPlayers = () => {
        // const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'get',
            url: baseUrl + '/api/fut/all-players-of-team/' + params.teamId + '/',
            // headers: { 'Authorization': token },
        }).then((response) => {
                console.log('loged users players: ', response.data);

                fetchGoals(response.data);

                // setPlayers(response.data);
            })
            .catch((error) => {
                console.log(error);
            })
    }

    const fetchGoals = (_players) => {
        // const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios.get(baseUrl + '/api/fut/goals-of-team/' + params.teamId + '/')
            .then((response) => {
                // console.log('logged user goals: ', response.data);
                // setGoals(response.data);
                const team_goals = response.data;
                let _totalGoals = 0;
                

                team_goals.map((item) => {
                    // item is an array of goals for single player
                    const PlayerId = item[0].player;
                    const PlayersIndex = _players.findIndex(element => element.id === PlayerId)
                    _totalGoals = _totalGoals + item.length;

                    if (_players[PlayersIndex].goals !== undefined) {
                        _players[PlayersIndex].goals = [..._players[PlayersIndex].goals, ...item];
                    }

                    _players[PlayersIndex].goals = [...item];
                })

                _players.map((item) => {
                    if (item.goals === undefined) {
                        item.goals = [];
                    }
                })

                setPlayers(_players);
                setTotalTeamGoals(_totalGoals);
            })
            .catch((error) => {
                console.log(error);
            })

    }

    const fetchTeam = () => {
        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'get',
            url: baseUrl + '/api/fut/team/',
            headers: { 'Authorization': token },
        }).then((response) => {
                console.log('function fetch team: logged users team: ', response.data);
                console.log('response.data.length: ', response.data.length);

                if (response.data.length !== 0) {
                    console.log('team exists...');

                    setTeam(response.data);
                }
                else {
                    console.log('sorry you have no team');
                }
            })
            .catch((error) => {
                console.log(error);
                
            })
    }

    const renderPlayers = () => {
        let _players = players;

        _players.sort((a, b) => b.goals.length - a.goals.length);

        // console.log('function renderPlayers, player: ', _players);

        if (filterActivePlayers) {
            _players = _players.filter(element => element.status === "active")
        }

        if (filterForwardPlayer) {
            _players = _players.filter(element => element.position === "ST" || 
            element.position === "LS" || element.position === "RS" || element.position === "LF" || element.position === "RF" ||
            element.position === "MS")
        }

        if (filterMiddlePlayer) {
            _players = _players.filter(element => element.position === "ZM" || 
            element.position === "ZOM" || element.position === "LM" || element.position === "RM" || element.position === "ZDM" ||
            element.position === "LZM" || element.position === "RZM")
        }

        if (filterBackPlayer) {
            _players = _players.filter(element => element.position === "IV" || 
            element.position === "LV" || element.position === "RV" || element.position === "LIV" || element.position === "RIV" ||
            element.position === "LAV" || element.position === "RAV")
        }
        
        console.log('function renderPlayers, player: ', _players);

        return _players.map((item) => {
            const goals_total = item.goals.length;
            const goals_SB = item.goals.filter(element => element.competition === "SB").length;
            const goals_DR = item.goals.filter(element => element.competition === "DR").length;
            const goals_CH = item.goals.filter(element => element.competition === "CH").length;

            return <tr key={Math.random() * 1000000} className='isClickable'>
                <td>{item.first_name + ' ' + item.last_name}</td>
                <td>{item.position}</td>
                <td>{item.status}</td>
                <td>{goals_total}</td>
                <td>{goals_SB}</td>
                <td>{goals_DR}</td>
                <td>{goals_CH}</td>
            </tr>
        });
    }

    const activateFilterPosition = (position_group) => {
        if (position_group == 'forward') {
            setFilterForwardPlayer(!filterForwardPlayer);
            setFilterMiddlePlayer(false);
            setFilterBackPlayer(false);
        }
        if (position_group == 'middle') {
            setFilterForwardPlayer(false);
            setFilterMiddlePlayer(!filterMiddlePlayer);
            setFilterBackPlayer(false);
        }
        if (position_group == 'back') {
            setFilterForwardPlayer(false);
            setFilterMiddlePlayer(false);
            setFilterBackPlayer(!filterBackPlayer);
        }
    }

    useEffect(() => {
        /* after loading page this function runs first */
        console.log('*** PAGE TEAM OVERVIEW / STATISTICS ***')

        fetchPlayers();
        fetchTeam();
        // checkToken();
        // }
    }, [pageloaded]);

    return (<div>
        <TopMenu />
        <div>TEAM STATISTICS</div>
        <div style={{ margin: 30 }}></div>
        {/* <div>TEAM: <strong>{team[0].name == undefined ? 'undefined' : team[0].name}</strong></div> */}
        <div>GOALS TOTAL: <strong>{totalTeamGoals}</strong> </div>
        <div style={{ margin: 30 }}></div>
        <div>Click Filter below to activate:</div>
        <input type='button' value='Active Players' className={filterActivePlayers ? 'isFilterActive' : 'isFilterInactive'} onClick={() => setFilterActivePlayers(!filterActivePlayers)} />
        <input type='button' value='Forward Players' className={filterForwardPlayer ? 'isFilterActive' : 'isFilterInactive'} onClick={() => activateFilterPosition('forward')} />
        <input type='button' value='Middle Players' className={filterMiddlePlayer ? 'isFilterActive' : 'isFilterInactive'} onClick={() => activateFilterPosition('middle')} />
        <input type='button' value='Back Players' className={filterBackPlayer ? 'isFilterActive' : 'isFilterInactive'} onClick={() => activateFilterPosition('back')} />
        <div style={{ margin: 30 }}></div>
        <table className='isTable'>
            <thead>
                <tr>
                    <th>Player name</th>
                    <th>Position</th>
                    <th>Status</th>
                    <th>Goals</th>
                    <th>SB</th>
                    <th>DR</th>
                    <th>CH</th>
                </tr>
            </thead>
            <tbody>
                {renderPlayers()}
            </tbody>
        </table>
    </div>);
}

export default TeamOverview;