import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import TopMenu from '../../components/top-menu';
import PlayerSmall from '../../components/player-small';
import ModalPlayers from '../../components/modal-players';
import "./index.scss";
/*
    page shows list of players of logged user
    possible to add goals, date and competition
    and save it to database
*/

const AddGoals = () => {
    const params = useParams();
    const axios = require('axios');
    const navigate = useNavigate();

    const getBaseUrl = () => {
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // dev code
            return 'http://127.0.0.1:8000';
        } else {
            // production code
            return 'https://fut-v2.herokuapp.com';
        }
    }

    const [baseUrl, setBaseUrl] = useState(getBaseUrl());
    const [pageloaded, setPageLoaded] = useState(0);
    const [loggedUser, setLoggedUser] = useState({ username: "" });
    const [players, setPlayers] = useState([]);
    const [gamesDate, setGamesDate] = useState("");
    const [competition, setCompetition] = useState("");
    const [divisionNumber, setDivisionNumber] = useState("");
    const [oponnentStrength, setOponnentStrength] = useState(80);
    const [difficultyLevel, setDifficultyLevel] = useState("");
    const [showPlayers, setShowPlayers] = useState(false)
    const [selectedPlayers, setSelectedPlayers] = useState([]);
    const [goals, setGoals] = useState([]);
    const [assistance, setAssistance] = useState([]);

    const fetchPlayers = () => {
        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'get',
            url: baseUrl + '/api/fifa/user/club/get-my-players/',
            headers: { 'Authorization': token },
        }).then((response) => {
            // console.log('players: ', response.data);

            let _players = response.data.data;

            setPlayers(_players);
        })
            .catch((error) => {
                console.log(error);
            })
    }
    const saveScore = () => {
        console.log("saving data...");

        let dateElem = document.getElementById("game-date");
        let divisionNumber = document.getElementById("division-number");

        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        if (gamesDate === '') {
            alert('you have to select date');
            return 0;
        }
        if (competition === '') {
            alert('you have to select competition');
            return 0;
        }
        if (competition === 'SB') {
            if (difficultyLevel === '') {
                alert('you have to select difficulty level');
                return 0;
            }
            if (oponnentStrength < 1) {
                alert('oponnent level cannot be smaller than 1');
                return 0;
            }
        }
        if (competition === 'DR') {
            if (divisionNumber.value == '') {
                alert('you have to select division number');
                return 0;
            }
        }

        if (selectedPlayers.length === 0) {
            alert('you have to select a player');
            return 0;
        }
        selectedPlayers.map((item, index) => {
            let nrGoals = document.getElementById("goals#" + index).value;
            let nrAssistances = document.getElementById("assistence#" + index).value;

            console.log(`player's id: ${item.id}; goals: ${nrGoals}; assistances: ${nrAssistances}`)

            if (parseInt(nrGoals) == 0 && parseInt(nrAssistances) == 0) {
                alert('player without points can not be saved');
                return 0;
            }

            for (let i = 0; i < nrGoals; i++) {

                axios({
                    method: 'post',
                    url: baseUrl + '/api/fifa/point/add/',
                    headers: { 'Authorization': token },
                    data: {
                        date: dateElem.value,
                        type: "GOAL",
                        competition: competition,
                        DR_league: divisionNumber.value,
                        SB_oponnent_strength: oponnentStrength,
                        SB_difficulty: difficultyLevel,
                        player: item.id
                    }
                }).then((response) => {
                }).catch((error) => { console.log(error); })
            }

            for (let i = 0; i < nrAssistances; i++) {

                axios({
                    method: 'post',
                    url: baseUrl + '/api/fifa/point/add/',
                    headers: { 'Authorization': token },
                    data: {
                        date: dateElem.value,
                        type: "ASSIST",
                        competition: competition,
                        DR_league: divisionNumber.value,
                        SB_oponnent_strength: oponnentStrength,
                        SB_difficulty: difficultyLevel,
                        player: item.id
                    }
                }).then((response) => {
                }).catch((error) => { console.log(error); })
            }


        })

        selectedPlayers.map((item, index) => {
            document.getElementById("goals#" + index).value = 0;
            document.getElementById("assistence#" + index).value = 0;
        })

        alert('scored saved succesfully..');
    }
    const checkToken = () => {
        console.log('calling function checkToken...');

        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'get',
            url: baseUrl + '/api/fut/user/',
            headers: { 'Authorization': token },
        }).then((response) => {
            // console.log("loggedUser: ", response.data);

            // TODO: refresh token 

            setLoggedUser(response.data);
        }).catch((error) => {
            console.log(error);

            navigate("/")
        })
    }
    const showState = () => {
        // console.log("players: ", players);
        // console.log("selectedPlayers: ", selectedPlayers);
        // console.log("goals: ", goals);
        // console.log("assistance: ", assistance);
        console.log("gamesDate: ", gamesDate);
        console.log("competition: ", competition);
        console.log("difficultyLevel: ", difficultyLevel);
        console.log("oponnentStrength: ", oponnentStrength);
        console.log("divisionNumber: ", divisionNumber);
        // console.log("competition: ", competition);
    }
    const getSelectedPlayers = (players) => {
        setSelectedPlayers([...players]);

        let newGoals = [];
        let newAssistance = [];

        players.map((item) => {
            newGoals.push(0);
            newAssistance.push(0);
        })

        setGoals([...newGoals]);
        setAssistance([...newAssistance]);

        setShowPlayers(false);
    }
    useEffect(() => {
        console.log("PAGE fut/add-score...");
        checkToken();
        fetchPlayers();
    }, [pageloaded]);

    return (<div>
        {showPlayers ? <ModalPlayers players={players} selectedPlayers={selectedPlayers} close={getSelectedPlayers} /> : <div></div>}
        <TopMenu />
        <div className='add-goals'>
            <div className='middle-container'>
                <div className='faked-opacity'></div>
                <div className='used-area'>

                    <strong onClick={showState}>ADD POINTS SCORED IN FIFA ULTIMATE TEAM:</strong>
                    <div className='row'>
                        <div>PART 1 : GAME DATA: </div>
                        <div></div>
                    </div>
                    <div className='row'>
                        <div>DATE:</div>
                        <div><input className='date' type="date" id="game-date" name="game-date" value={gamesDate} onChange={(e) => setGamesDate(e.target.value)}></input></div>
                    </div>
                    <div className='row'>
                        <div>COMPETITION:</div>
                        <select className='dropdown' name="competition" id="competition" onChange={(e) => setCompetition(e.target.value)}>
                            <option value="">--Please choose a competition--</option>
                            <option value="SB" onChange={(e) => setCompetition(e.target.value)}>Squad Battles</option>
                            <option value="DR" onChange={(e) => setCompetition(e.target.value)}>Division Rivals</option>
                            <option value="CH" onChange={(e) => setCompetition(e.target.value)}>Champions</option>
                        </select>
                    </div>
                    <div className={competition === "DR" ? 'row' : "row hidden"}>
                        <div>DIVISION NUMBER:</div>
                        <select className='dropdown' id="division-number" name="division-number" onChange={(e) => setDivisionNumber(e.target.value)}>
                            <option value="">--Please choose a division--</option>
                            <option value="ED" onChange={(e) => setDivisionNumber(e.target.value)}>ELITE DISION</option>
                            <option value="D1" onChange={(e) => setDivisionNumber(e.target.value)}>DIVISION 1</option>
                            <option value="D2" onChange={(e) => setDivisionNumber(e.target.value)}>DIVISION 2</option>
                            <option value="D3" onChange={(e) => setDivisionNumber(e.target.value)}>DIVISION 3</option>
                            <option value="D4" onChange={(e) => setDivisionNumber(e.target.value)}>DIVISION 4</option>
                            <option value="D5" onChange={(e) => setDivisionNumber(e.target.value)}>DIVISION 5</option>
                            <option value="D6" onChange={(e) => setDivisionNumber(e.target.value)}>DIVISION 6</option>
                            <option value="D7" onChange={(e) => setDivisionNumber(e.target.value)}>DIVISION 7</option>
                            <option value="D8" onChange={(e) => setDivisionNumber(e.target.value)}>DIVISION 8</option>
                            <option value="D9" onChange={(e) => setDivisionNumber(e.target.value)}>DIVISION 9</option>
                            <option value="D10" onChange={(e) => setDivisionNumber(e.target.value)}>DIVISION 10</option>
                        </select>
                    </div>
                    <div className={competition === "SB" ? 'row' : "row hidden"}>
                        <div>DIFFICULTY LEVEL:</div>
                        <select className='dropdown' name="oponnent-strength" id="oponnent-strength" onChange={(e) => setDifficultyLevel(e.target.value)}>
                            <option value="N/A">--Please choose a level--</option>
                            <option value="Amateur" onChange={(e) => setDifficultyLevel(e.target.value)}>AMATEUR</option>
                            <option value="Beginner" onChange={(e) => setDifficultyLevel(e.target.value)}>BEGINNER</option>
                            <option value="Semi-Pro" onChange={(e) => setDifficultyLevel(e.target.value)}>SEMI-PRO</option>
                            <option value="Pro" onChange={(e) => setDifficultyLevel(e.target.value)}>PRO</option>
                            <option value="World Class" onChange={(e) => setDifficultyLevel(e.target.value)}>WORLD CLASS</option>
                            <option value="Legend" onChange={(e) => setDifficultyLevel(e.target.value)}>LEGEND</option>
                            <option value={difficultyLevel} onChange={(e) => setDifficultyLevel(e.target.value)}>ULTIMATE</option>
                        </select>
                    </div>
                    <div className={competition === "SB" ? 'row' : "row hidden"}>
                        <div>OPONNENT STRENGTH:</div>
                        <input className='date' type="number" id="game-date" name="game-date" value={oponnentStrength} onChange={(e) => setOponnentStrength(e.target.value)}></input>
                    </div>
                    <div className='row'>
                        <div>PART 2 : PLAYERS DATA: </div>
                    </div>
                    <div className='row'>
                        <div className='players-container'>
                            <div className='row-score'>
                                <div className='col_1'><div className='isClickable' onClick={() => setShowPlayers(true)}>Add Player</div></div>
                                <div className='col' >Scored Goals</div>
                                <div className='col' >Assistance</div>
                            </div>
                            {selectedPlayers.map((item, index) => {
                                return <div className='row-score'>
                                    <div className='col_1'>{item.name}</div>
                                    <div className='col'><input type="number" className='input-score' id={`goals#${index}`} name={`goals#${index}`} min={0} defaultValue={0} /></div>
                                    <div className='col'><input type="number" className='input-score' id={`assistence#${index}`} name={`assistence#${index}`} min={0} defaultValue={0} /></div>
                                </div>
                            })}
                        </div>
                    </div>

                    <div><input type='button' value="SAVE SCORE" className='isClickable' onClick={() => saveScore()} /></div>
                </div>
            </div>
        </div>
    </div>);
}

export default AddGoals;