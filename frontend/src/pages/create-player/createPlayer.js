import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';

const CreatePlayer = () => {
    const axios = require('axios');
    const navigate = useNavigate();

    const getBaseUrl = () => {
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // dev code
            return 'http://127.0.0.1:8000';
        } else {
            // production code
            return 'https://fut-v2.herokuapp.com';
        }
    }

    const [pageloaded, setPageLoaded] = useState(0);
    const [users, setUsers] = useState([]);
    const [teams, setTeams] = useState([]);
    const [players, setPlayers] = useState([]);
    const [tokenIsValid, setTokenIsValid] = useState(false);
    const [baseUrl, setBaseUrl] = useState(getBaseUrl());

    const [first_name, setfirst_name] = useState('');
    const [middle_name, setmiddle_name] = useState('');
    const [last_name, setlast_name] = useState('');
    const [position, setposition] = useState('ST');
    const [status, setstatus] = useState('active');
    const [avatar_large, setavatar_large] = useState('');
    const [avatar_small, setavatar_small] = useState('');

    const verifyToken = (token) => {
        if (token !== null && tokenIsValid === false) {
            axios({
                method: 'post',
                url: baseUrl + '/api/token/verify/',
                data: {
                    token: token
                }
            })
                .then((response => {
                    setTokenIsValid(true);
                    return true;
                }))
                .catch((error) => {
                    console.log(error);

                    return false;
                })
        }
        else if (token === null) {
        }

        return false;
    }

    const loggOut = () => {
        localStorage.removeItem('FUT_user_token');
        localStorage.removeItem('FUT_user_first_name');
        localStorage.removeItem('FUT_user_last_name');
        localStorage.removeItem('FUT_username');
        setTokenIsValid(false);

        alert('Loggout succesfully');
    }

    const saveNewPlayer = () => {

        if (tokenIsValid) {
            const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

            axios({
                method: 'post',
                url: baseUrl + '/api/fut/new-player/',
                headers: { 'Authorization': token },
                data: {
                    first_name: first_name,
                    middle_name: middle_name,
                    last_name: last_name,
                    position: position,
                    status: status,
                    avatar_large: avatar_large,
                    avatar_small: avatar_small,
                }
            })
                .then((response => {
                    alert('new player created!');
                }))
                .catch((error) => {
                    console.log(error);
                })
        }
        else {
            alert('you are not logged in. Please log in or create account.')
        }
    }

    const sayHello = () => {
        if (tokenIsValid) {
            const first_name = localStorage.getItem('FUT_user_first_name');
            const last_name = localStorage.getItem('FUT_user_last_name');
            const username = localStorage.getItem('FUT_username');
            const greeting = 'Hello ' + username;

            return <div>
                {greeting}
                <div onClick={() => loggOut()} ><span className='isClickable'>Loggout</span></div>
                <Link to="/team/create-new-player" className='isClickable' >Create New Player</Link>
            </div>
        }
        else {
            return <div>Hello Guest, please <Link to="/log-in/" className='isClickable' >LOG-IN</Link> or <Link to="/sign-in/" className='isClickable' >SIGN-IN</Link></div>
        }
    }

    useEffect(() => {
        const token = localStorage.getItem('FUT_user_token');
        verifyToken(token);
    }, [pageloaded]);

    return (<div className='player'>
        <div>Page create new player:</div>
        <div style={{ margin: 30 }}></div>
        {/* <form method='POST' action={baseUrl + '/api/players/'} encType='multipart/form-data' onSubmit={handleSubmit} > */}
        <div className='isRow'>
            <div className='isColumn'>
                <label htmlFor='first_name'>first name: </label>
                <label htmlFor='middle_name'>middle name: </label>
                <label htmlFor='last_name'>last name: </label>
                <label htmlFor='position'>position: </label>
                <label htmlFor='status'>status: </label>
                <label htmlFor='avatar_large'>large avatar: </label>
                <label htmlFor='avatar_small'>small avatar: </label>
            </div>
            <div className='isColumn'>
                <input type='text' id='first_name' name='first_name' required value={first_name} onChange={(e) => setfirst_name(e.target.value)} className='border-black border' />
                <input type='text' id='middle_name' name='middle_name' value={middle_name} onChange={(e) => setmiddle_name(e.target.value)} className='border-black border' />
                <input type='text' id='last_name' name='last_name' required value={last_name} onChange={(e) => setlast_name(e.target.value)} className='border-black border' />
                <input type='text' id='position' name='position' required value={position} onChange={(e) => setposition(e.target.value)} className='border-black border' />
                <input type='text' id='status' name='status' required value={status} onChange={(e) => setstatus(e.target.value)} className='border-black border' />
                <input type='file' id='avatar_large' name='avatar_large' onChange={(e) => setavatar_large(parseInt(e.target.value))} />
                <input type='file' id='avatar_small' name='avatar_small' onChange={(e) => setavatar_small(parseInt(e.target.value))} />
            </div>
        </div>
        <div><input type='button' value='Save new Player' className='isClickable' onClick={() => saveNewPlayer()} /></div>
        {/* </form> */}
        <input type='button' onClick={() => navigate('/')} value='Landing page' className='isClickable' />
        <div style={{ margin: 30 }}></div>
        <div>{sayHello()}</div>
    </div>);
}

export default CreatePlayer;