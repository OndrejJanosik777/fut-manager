import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import NavBar from '../../components/nav-bar';

const Team = () => {
    const localUrl = 'http://127.0.0.1:8000';
    const herokuUrl = 'https://fut-manager-api.herokuapp.com';

    // page loaded - fake state - just to run first hook at start...
    const [loaded, setLoaded] = useState(0);
    // local state of component
    const [players, setPlayers] = useState([]);
    const [baseUrl, setBaseUrl] = useState(herokuUrl);

    // fetch all players from database
    const getAllPlayersFromDatabase = () => {
        const axios = require('axios');

        const url = baseUrl + "/api/players/";

        axios.get(url)
            .then((response) => {
                console.log('response: ', response);

                let allPlayers = response.data;
                let activePlayers = allPlayers.filter(player => player.position != 'BA')
                activePlayers.sort((a, b) => b.scored_goals - a.scored_goals);

                setPlayers(activePlayers);
            })
            .catch((error) => {
                console.log('error: ', error);
            })
    }

    // hooks for component did mount
    // this hook will only run once - to load players from backend database...
    useEffect(() => {
        getAllPlayersFromDatabase();
    }, [loaded])

    // object with css properties
    const pictureStyle = (pictureUrl) => {
        console.log('pictureUrl: ', pictureUrl);

        return {
            width: '64px',
            height: '75px',
            backgroundColor: 'white',
            backgroundImage: 'url(' + pictureUrl + ')',
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            borderRadius: '15px',
        }
    }

    const plotFields = (rows, columns) => {
        // create array representation to map later
        let column = [];
        for (let i = 0; i < rows; i++) column.push(0);

        let row = [];
        for (let i = 0; i < columns; i++) row.push(column);

        // console.log('row: ', row);

        return <div className='flex h-full'>
            {row.map((r) => {
                return <div key={Math.random() * 100000} className='p-1 flex flex-col justify-around items-center'>
                    {r.map((c) => {
                        return <div key={Math.random() * 100000} className='w-4 h-4 border-solid border border-gray-500 cursor-pointer hover:bg-gray-400'></div>
                    })}
                </div>
            })}
        </div>;
    }

    const playerRowStyle = (index) => {
        if (index % 2 === 0) {
            return 'h-20 bg-gray-100 flex items-center';
        }
        else {
            return 'h-20 bg-gray-200 flex items-center'
        }
    }

    return (<div className='team'>
        <header>
            <NavBar activeField='1' />
        </header>
        <main className='flex justify-center'>
            <section className='w-full max-w-7xl m-4'>
                <div className='w-full '>
                    <ul>
                        <li className='h-16 bg-gray-300 flex items-center border-solid border border-gray-500'>
                            <div className='w-40 p-3'>Games played:</div>
                            <div className='w-16'></div>
                            <div className='w-5'></div>
                            {plotFields(2, 43)}
                        </li>
                        {players.map((player, index) => {
                            return <li key={Math.random() * 100000} className={playerRowStyle(index)}>
                                <div className='w-40 p-3'>{player.name}</div>
                                <div style={pictureStyle(player.image)}></div>
                                <div className='w-5'></div>
                                {plotFields(3, 43)}
                            </li>
                        })}
                    </ul>
                </div>

            </section>
        </main>
    </div>);
}

export default Team;