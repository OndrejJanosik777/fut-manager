import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import moment from 'moment';
import TopMenu from '../../components/top-menu';
import editPicture from './assets/pencil-square.svg';
import deletePicture from './assets/trash3.svg';
import NavBar from '../../components/nav-bar';
import Modal_Delete from '../../modal-components/delete';
import './index.scss';

const ManageTeams = () => {
    const params = useParams();
    const axios = require('axios');
    const navigate = useNavigate();

    const getBaseUrl = () => {
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // dev code
            return 'http://127.0.0.1:8000';
        } else {
            // production code
            return 'https://fut-v3.herokuapp.com';
        }
    }

    const [baseUrl, setBaseUrl] = useState(getBaseUrl());
    const [fetchedData, set_fetchedData] = useState({ 
        count: 0,
        next: null,
        previous: null,
        results: []
    });
    const [displayModal_Delete, set_displayModal_Delete] = useState(false);
    const [activeItemId, setActiveItemId] = useState(0);
    const [activeSorting, set_activeSorting] = useState('P');
    const [token, setToken] = useState("Bearer " + localStorage.getItem('FUT_user_token_access'));
    const [token_refresh, setTokenRefresh] = useState(localStorage.getItem('FUT_user_token_refresh'));
    const [spin_generatingNewTeam, setSpin_generatingNewTeam] = useState(false);
    const [updateMode, setUpdateMode] = useState(false);
    const [user, setUser] = useState({
        username: "",
        first_name: "",
        last_name: "",
        date_joined: "",
        last_login: "",
    });

    useEffect(() => {
        console.log('page manage-teams loaded...')
        checkToken();
        getUserData();
        fetchTeams();
    }, []);

    const checkToken = () => {
        console.log('function refresh token ...');

        axios({
            method: 'post',
            url: baseUrl + '/api/token/refresh/',
            data: {
                refresh: token_refresh
            }
        })
            .then((response => {
                console.log('token refreshed succesfully');

                localStorage.setItem('FUT_user_token_access', response.data.access);

                setToken("Bearer " + response.data.access);
            }))
            .catch((error) => {
                console.log(error);

                alert('function checktoken: not valid credentials...');

                navigate('/');
            })
    }

    const getUserData = () => {
        console.log('function getUserData  ...');

        axios({
            method: 'get',
            url: baseUrl + '/api/fifa/me',
            headers: {
                "Authorization": token
            }
        })
            .then((response => {
                console.log('user info got succesfully succesfully');

                console.log("user: ", response.data);

                setUser(response.data);
            }))
            .catch((error) => {
                console.log(error);

                alert('not valid credentials...');

                navigate('/');
            })
    }

    const fetchTeams = () => {
        axios({
            method: 'get',
            url: baseUrl + '/api/fifa/teams/',
            headers: { 
                "Authorization": token
            }
        }).then((response) => {
            console.log("fetchTeams: ", response.data);

            // initial sorting: nr of players
            let newData = response.data;

            newData.results.sort((a, b) => {
                return b.players.length - a.players.length;
            })

            set_fetchedData(newData);

            setSpin_generatingNewTeam(false);

            set_displayModal_Delete(false);

            setUpdateMode(false);
        }).catch((error) => {
            console.log("Problem with fetching teams ", error);
        })

    }

    const createTeam = () => {
        setSpin_generatingNewTeam(true);

        let name = document.getElementById('teamName').value;

        axios({
            method: 'post',
            url: baseUrl + '/api/fifa/teams/',
            headers: { 'Authorization': token },
            data: {
                name: name,
            }
        }).then((response) => {
            console.log("team created: ", response.data);

            fetchTeams();

            setSpin_generatingNewTeam(false);

            document.getElementById('teamName').value = "";
        }).catch((error) => {
            alert('problem with creating new team.');

            console.log(error);

            setSpin_generatingNewTeam(false);
        })
    }

    const deleteItem = (id) => {
        setActiveItemId(id);

        set_displayModal_Delete(true);
    }

    const deleteItemFromBackend = () =>{
        // setActiveItemId(id);

        // console.log('deleting from backend team with id: ', activeItemId);

        axios({
            method: 'delete',
            url: baseUrl + '/api/fifa/teams/' + activeItemId + '/',
            headers: { 'Authorization': token },
        }).then((response) => {
            fetchTeams();
        }).catch((error) => {
            console.log('Problem with deleting team: ', error)

            set_displayModal_Delete(!displayModal_Delete);
        })
    }

    const updateItem = (id) => {
        let selectedTeam = fetchedData.results.find(element => element.id === id);

        setActiveItemId(id);

        setUpdateMode(true);

        document.getElementById('teamName').value = selectedTeam.name;
    }

    const updateTeamInDatabase = () => {
        setSpin_generatingNewTeam(true);

        console.log('updating team...', activeItemId);

        let selectedTeam = fetchedData.results.find(element => element.id === activeItemId);
        selectedTeam.name = document.getElementById('teamName').value;

        document.getElementById('teamName').value = "";

        axios({
            method: 'patch',
            url: baseUrl + '/api/fifa/teams/' + activeItemId + '/',
            headers: { 'Authorization': token },
            data: {
                name: selectedTeam.name,
            }
        }).then((response) => {
            fetchTeams();
        }).catch((error) => {
            console.log('Problem with deleting team: ', error)

            set_displayModal_Delete(!displayModal_Delete);
        })
    }

    const showState = () => {
        console.log('showing component state...');

        console.log('activeItemId: ', activeItemId);
    }

    return (<div className='manage-your-teams'>
        { displayModal_Delete ?
            <Modal_Delete text={'Are you sure to delete team?'} actionYes={deleteItemFromBackend} actionNo={() => set_displayModal_Delete(!displayModal_Delete)} /> :
            <div></div>
         }
        <NavBar user={user} />
        <h1 className='h_1' onClick={() => showState()}>FUT Teams: {fetchedData.count}</h1>
        <main className='main'>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col" className='w-45'>Team</th>
                        <th scope="col" className={activeSorting == 'G' ? 'w-15 active-column' : 'w-15'}>G</th>
                        <th scope="col" className={activeSorting == 'S' ? 'w-15 active-column' : 'w-15'}>S</th>
                        <th scope="col" className={activeSorting == 'P' ? 'w-15 active-column' : 'w-15'}>P</th>
                        <th scope="col" className='w-10'></th>
                        <th scope="col" className='w-10'></th>
                    </tr>
                </thead>
                <tbody>
                    {fetchedData.results.map((item) => {
                        return  <tr key={Math.random() * 100000}>
                                    <td>{item.name}</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>{item.players.length}</td>
                                    <td><img src={editPicture} alt={""} onClick={() => updateItem(item.id)} /></td>
                                    <td><img src={deletePicture} alt={""} onClick={() => deleteItem(item.id)} /></td>
                                </tr>
                    })}
                </tbody>
            </table>
        </main>
        <div className='buttons-container' >
            <input className='input-field_1' type='text' id='teamName' name='teamName' placeholder='team name' />
            {
                spin_generatingNewTeam ? 
                <div className="spinner-border" role="status"></div> :
                <input 
                    type='button' 
                    value={updateMode ? 'Update Name' : 'Create Team'} 
                    className='button_1' 
                    onClick={updateMode ? () => updateTeamInDatabase() : () => createTeam()} 
                />
            }
            {
                updateMode && !spin_generatingNewTeam ?
                <input 
                    type='button' 
                    value={'Manage Team'} 
                    className='button_1' 
                    onClick={() => navigate(`/manage-team?team=${activeItemId}`)} 
                />            
                :
                <div></div>
            }
        </div>
    </div>);
}

export default ManageTeams;