import axios from 'axios';
import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import NavBar from '../../components/nav-bar';

const Statistics = () => {
    const localUrl = 'http://127.0.0.1:8000';
    const herokuUrl = 'https://fut-manager-api.herokuapp.com';

    // page loaded - fake state - just to run first hook at start...
    const [loaded, setLoaded] = useState(0);
    // local state of component
    const [players, setPlayers] = useState([]);
    const [baseUrl, setBaseUrl] = useState(herokuUrl);

    // hook navigate
    const navigate = useNavigate();

    // fetch all players from database
    const getAllPlayersFromDatabase = () => {
        // idea is to fetch all players from database
        // and filter one with at least one goal
        const axios = require('axios');

        const url = baseUrl + "/api/players/";

        axios.get(url)
            .then((response) => {
                // console.log('response: ', response);

                // get all players
                const allPlayers = response.data;
                // filter for players with at least 1 scored goal
                let succesfullPlayers = allPlayers.filter(player => player.scored_goals > 0);
                // sort accorsing ratio scored_goals / player_games
                succesfullPlayers.sort((a, b) => b.scored_goals / b.played_games - a.scored_goals / a.played_games);


                setPlayers(succesfullPlayers);
            })
            .catch((error) => {
                console.log('error: ', error);
            })


    }

    // this hook will only run once - to load players from backend database...
    useEffect(() => {
        getAllPlayersFromDatabase();
    }, [loaded])

    const pictureStyle = (pictureUrl) => {
        // console.log('pictureUrl: ', pictureUrl);

        return {
            width: '85px',
            height: '100px',
            backgroundColor: 'white',
            backgroundImage: 'url(' + pictureUrl + ')',
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            borderRadius: '15px',
        }
    }

    return (<div>
        <header>
            <NavBar activeField='2' />
        </header>
        <main className='w-full flex flex-col  justify-center items-center mt-8 mb-8'>
            <p className='w-full max-w-screen-xl m-2'>Note: ratio is calculated as scored goals divided by played games</p>
            <table className='w-full border-black border max-w-screen-xl'>
                <thead className='border-black border'>
                    <tr className='border-black border'>
                        <th>Position</th>
                        <th>Name</th>
                        <th>Ratio</th>
                        {/* <th>Birthday</th> */}
                        <th>Games played</th>
                        <th>Goals scored</th>
                        <th>Position</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {players.map((player, index) => {
                        let ratio = parseInt(player.scored_goals) / parseInt(player.played_games);
                        return <tr key={Math.random() * 100000} className="border-black border" >
                            <td className='text-center p-2 text-3xl font-bold border-black border'>{index + 1}</td>
                            <td className='p-2 text-xl border-black border'>{player.name}</td>
                            <td className='text-center text-2xl font-bold border-black border'>{Math.round(ratio * 1000) / 1000}</td>
                            {/* <td className='text-center text-xl border-black border'>{player.birthday}</td> */}
                            <td className='text-center text-xl border-black border'>{player.played_games}</td>
                            <td className='text-center text-xl border-black border'>{player.scored_goals}</td>
                            <td className='text-center text-xl border-black border'>{player.position}</td>
                            <td className='flex justify-center text-xl items-center'><div style={pictureStyle(player.image)}></div></td>
                        </tr>
                    })}
                </tbody>
            </table>
        </main>
        {/* <div className='temporary'>
            <div>Local state: </div>
            <div onClick={() => console.log('local state: players: ', players)}>players: </div>
        </div> */}
    </div>);
}

export default Statistics;