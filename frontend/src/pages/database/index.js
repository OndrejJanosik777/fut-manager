import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import PlayerInfo from '../../components/player-info';
import TopMenu from '../../components/top-menu';
import '../index.scss';
import './index.scss';

const Database = () => {
    const params = useParams(); 
    const axios = require('axios');
    const navigate = useNavigate();

    const getBaseUrl = () => {
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // dev code
            return 'http://127.0.0.1:8000';
        } else {
            // production code
            return 'https://fut-v2.herokuapp.com';
        }
    }

    const [baseUrl, setBaseUrl] = useState(getBaseUrl());
    const [loggedUser, setLoggedUser] = useState({ username: '' });
    const [pageloaded, setPageLoaded] = useState(0);
    const [players, setPlayers] = useState([]);
    const [leagues, setLeagues] = useState([]);
    const [nations, setNations] = useState([]);
    const [playersInMyClub, setPlayersInMyClub] = useState([]);
    const [clubs, setClubs] = useState([]);
    const [filteredClubs, setFilteredClubs] = useState([]);
    const [selectedleague, setSelectedLeague] = useState('--- none ---');
    const [selectedNation, setSelectedNation] = useState('--- none ---');
    const [selectedClub, setSelectedClub] = useState('--- none ---');
    const [playersName, setPlayersName] = useState('');
    const [amountPlayers, setAmountPlayers] = useState(0);
    const [activePage, setActivePage] = useState(0);
    const [extendedFilter, setExtendedFilter] = useState('shooting')
    const [extendedSearch, setExtendedSearch] = useState(false);

    const checkToken = () => {
        console.log('function checkToken...');

        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'get',
            url: baseUrl + '/api/fut/user/',
            headers: { 'Authorization': token },
        }).then((response) => {
            console.log("loggedUser: ", response.data);

            // TODO: refresh token 

            setLoggedUser(response.data);

            return true;
        }).catch((error) => {
            console.log(error);

            return false;
        })
    }
    const fetchPlayers = (pageNumber) => {
        const ligue = leagues.find((element) => element.name === selectedleague)
        const club = clubs.find((element) => element.name === selectedClub)
        const nation = nations.find((element) => element.name === selectedNation)
        let queryParams = "";

        console.log('ligue: ', ligue);
        console.log('nation: ', nation);
        console.log('club: ', club);

        if (ligue !== undefined) {
            if (queryParams === '')
                queryParams = queryParams + "?league=" + ligue.league_id;
            else
                queryParams = queryParams + "&league=" + ligue.league_id;
        }
        if (nation !== undefined) {
            if (queryParams === '')
                queryParams = queryParams + "?nation=" + nation.nation_id;
            else
                queryParams = queryParams + "&nation=" + nation.nation_id;
        }
        if (club !== undefined) {
            if (queryParams === '')
                queryParams = queryParams + "?club=" + club.club_id;
            else
                queryParams = queryParams + "&club=" + club.club_id;
        }
        if (playersName !== '') {
            if (queryParams === '')
                queryParams = queryParams + "?name_icontains=" + playersName;
            else
                queryParams = queryParams + "&name_icontains=" + playersName;
        }
        if (queryParams === '')
            queryParams = queryParams + "?page=" + pageNumber;
        else
            queryParams = queryParams + "&page=" + pageNumber;

        const url = baseUrl + '/api/fifa/players' + queryParams;

        console.log('fetching from url: ', url);

        axios({
            method: 'get',
            url: url,
        }).then((response) => {
            console.log("players data: ", response.data.data);

            // TODO: refresh token 

            setPlayers(response.data.data);
            setAmountPlayers(response.data.count);
            // setSelectedLeague(dropDownLigue.value);
        }).catch((error) => {
            console.log(error);
        })
    }
    const fetchPlayersExtended = (pageNumber) => {
        const ligue = leagues.find((element) => element.name === selectedleague);
        const club = clubs.find((element) => element.name === selectedClub);
        const nation = nations.find((element) => element.name === selectedNation);

        const shootingAttributes = [
            'shooting_finishing_min', 'shooting_finishing_max',
            'shooting_shotPower_min', 'shooting_shotPower_max',
            'shooting_volleys_min', 'shooting_volleys_max',
            'shooting_longShots_min', 'shooting_longShots_max',
            'shooting_positioning_min', 'shooting_positioning_max',
            'shooting_penalties_min', 'shooting_penalties_max'
        ];

        const paceAttributes = [
            'acceleration_min', 'acceleration_max',
            'sprintSpeed_min', 'sprintSpeed_max'
        ];

        const passingAttributes = [
            'crossing_min', 'crossing_max',
            'vision_min', 'vision_max',
            'freeKickAccuracy_min', 'freeKickAccuracy_max',
            'shortPassing_min', 'shortPassing_max',
            'longPassing_min', 'longPassing_max',
            'curve_min', 'curve_max'
        ];

        const dribblingAttributes = [
            'agility_min', 'agility_max',
            'balance_min', 'balance_max',
            'reactions_min', 'reactions_max',
            'ballControl_min', 'ballControl_max',
            'dribbling_min', 'dribbling_max',
            'composure_min', 'composure_max'
        ];

        const defendingAttributes = [
            'interceptions_min', 'interceptions_max',
            'headingAccuracy_min', 'headingAccuracy_max',
            'standingTackle_min', 'standingTackle_max',
            'slidingTackle_min', 'slidingTackle_max',
            'defenseAwareness_min', 'defenseAwareness_max'
        ];

        const physicalityAttributes = [
            'jumping_min', 'jumping_max',
            'stamina_min', 'stamina_max',
            'strength_min', 'strength_max',
            'aggression_min', 'aggression_max'
        ];

        const goalAttributes = [
            'diving_min', 'diving_max',
            'handling_min', 'handling_max',
            'kicking_min', 'kicking_max',
            'positioning_min', 'positioning_max',
            'reflexes_min', 'reflexes_max'
        ];

        let queryParams = "";

        let attributes = [];

        if (extendedFilter === 'shooting') attributes = [...shootingAttributes];
        if (extendedFilter === 'pace') attributes = [...paceAttributes];
        if (extendedFilter === 'passing') attributes = [...passingAttributes];
        if (extendedFilter === 'dribling') attributes = [...dribblingAttributes];
        if (extendedFilter === 'defending') attributes = [...defendingAttributes];
        if (extendedFilter === 'physicality') attributes = [...physicalityAttributes];
        if (extendedFilter === 'goalkeeper') attributes = [...goalAttributes];

        attributes.map((item) => {
            if (document.getElementById(item).value !== '' && document.getElementById(item).value !== '0') {
                if (queryParams === '')
                    queryParams = queryParams + "?" + item + "=" + document.getElementById(item).value;
                else
                    queryParams = queryParams + "&" + item + "=" + document.getElementById(item).value;
            }
        })

        if (ligue !== undefined) {
            if (queryParams === '')
                queryParams = queryParams + "?league=" + ligue.league_id;
            else
                queryParams = queryParams + "&league=" + ligue.league_id;
        }

        if (nation !== undefined) {
            if (queryParams === '')
                queryParams = queryParams + "?nation=" + nation.nation_id;
            else
                queryParams = queryParams + "&nation=" + nation.nation_id;
        }

        if (club !== undefined) {
            if (queryParams === '')
                queryParams = queryParams + "?club=" + club.club_id;
            else
                queryParams = queryParams + "&club=" + club.club_id;
        }

        if (queryParams === '')
            queryParams = queryParams + "?page=" + pageNumber;
        else
            queryParams = queryParams + "&page=" + pageNumber;

        const url = baseUrl + '/api/fifa/' + extendedFilter + '-attributes' + queryParams;

        console.log('fetching from url: ', url);

        axios({
            method: 'get',
            url: url,
        }).then((response) => {
            console.log("players data: ", response.data.data);

            setPlayers(response.data.data);
            setAmountPlayers(response.data.count);
        }).catch((error) => {
            console.log(error);
        })
    }
    const fetchClubs = () => {
        axios({
            method: 'get',
            url: baseUrl + '/api/fifa/clubs',
        }).then((response) => {
            // console.log("fetchClubs: ", response.data);

            setClubs(response.data);
            setFilteredClubs(response.data);
        }).catch((error) => {
            console.log(error);
        })
    }
    const fetchLeagues = () => {
        axios({
            method: 'get',
            url: baseUrl + '/api/fifa/leagues',
        }).then((response) => {
            // console.log("fetchLeagues: ", response.data);

            setLeagues(response.data);
        }).catch((error) => {
            console.log(error);
        })
    }
    const fetchNations = () => {
        axios({
            method: 'get',
            url: baseUrl + '/api/fifa/nations',
        }).then((response) => {
            // console.log("fetchLeagues: ", response.data);

            setNations(response.data);
        }).catch((error) => {
            console.log(error);
        })
    }
    const dropDownLeaguesChanged = (activeLigueName) => {
        console.log('drop down Ligues changed...', activeLigueName);
        setSelectedLeague(activeLigueName);
        setSelectedClub('--- none ---');

        const league = leagues.find(elem => elem.name === activeLigueName);
        console.log('league.id: ', league.league_id);

        const _filteredClubs = clubs.filter(element => element.league_id === league.league_id)
        setFilteredClubs(_filteredClubs);
    }
    const dropDownClubsChanged = (activeClubName) => {
        setSelectedClub(activeClubName);
    }
    const dropDownNationsChanged = (activeClubName) => {
        setSelectedNation(activeClubName);
    }
    const showLocalState = () => {
        console.log('local state...');
        console.log('baseUrl: ', baseUrl);
        console.log('pageloaded: ', pageloaded);
        console.log('players: ', players);
        console.log('leagues: ', leagues);
        console.log('nations: ', nations);
        console.log('clubs: ', clubs);
        console.log('filteredClubs: ', filteredClubs);
        console.log('selectedleague: ', selectedleague);
        console.log('selectedNation: ', selectedNation);
        console.log('selectedClub: ', selectedClub);
        console.log('playersName: ', playersName);
        console.log('loggedUser: ', loggedUser);
        console.log('playersInMyClub: ', playersInMyClub);
    }
    const listToNextPage = () => {
        if ((activePage + 1) * 10 < amountPlayers) {
            let nextPage = activePage + 1;

            setActivePage(nextPage);

            if (extendedSearch) {
                fetchPlayersExtended(nextPage);
            }
            else {
                fetchPlayers(nextPage);
            }
        }
    }
    const listToPreviousPage = () => {
        if (activePage > 0) {
            let nextPage = activePage - 1;

            setActivePage(nextPage);

            if (extendedSearch) {
                fetchPlayersExtended(nextPage);
            }
            else {
                fetchPlayers(nextPage);
            }
        }
    }
    const showShootingFilter = () => {
        return <div className='extended-attributes-bottom'>
            <div className='group'>
                <div className="line">
                    <div className='label'>Finishing</div>
                    <div className='row'>
                        <div>Min:<input id='shooting_finishing_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='shooting_finishing_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>shot power</div>
                    <div className='row'>
                        <div>Min:<input id='shooting_shotPower_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='shooting_shotPower_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>volleys</div>
                    <div className='row'>
                        <div>Min:<input id='shooting_volleys_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='shooting_volleys_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>long shots</div>
                    <div className='row'>
                        <div>Min:<input id='shooting_longShots_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='shooting_longShots_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>positioning</div>
                    <div className='row'>
                        <div>Min:<input id='shooting_positioning_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='shooting_positioning_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>penalties</div>
                    <div className='row'>
                        <div>Min:<input id='shooting_penalties_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='shooting_penalties_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
            </div>
        </div>
    }
    const showPaceFilter = () => {
        return <div className='extended-attributes-bottom'>
            <div className='group'>
                <div className="line">
                    <div className='label'>acceleration</div>
                    <div className='row'>
                        <div>Min:<input id='acceleration_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='acceleration_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>sprint speed</div>
                    <div className='row'>
                        <div>Min:<input id='sprintSpeed_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='sprintSpeed_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
            </div>
        </div>
    }
    const showPassingFilter = () => {
        return <div className='extended-attributes-bottom'>
            <div className='group'>
                <div className="line">
                    <div className='label'>short passing</div>
                    <div className='row'>
                        <div>Min:<input id='shortPassing_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='shortPassing_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>long passing</div>
                    <div className='row'>
                        <div>Min:<input id='longPassing_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='longPassing_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>curve</div>
                    <div className='row'>
                        <div>Min:<input id='curve_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='curve_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>free kick accuracy</div>
                    <div className='row'>
                        <div>Min:<input id='freeKickAccuracy_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='freeKickAccuracy_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>crossing</div>
                    <div className='row'>
                        <div>Min:<input id='crossing_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='crossing_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>vision</div>
                    <div className='row'>
                        <div>Min:<input id='vision_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='vision_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
            </div>
        </div>
    }
    const showDriblingFilter = () => {
        return <div className='extended-attributes-bottom'>
            <div className='group'>
                <div className="line">
                    <div className='label'>ball control</div>
                    <div className='row'>
                        <div>Min:<input id='ballControl_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='ballControl_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>dribbling</div>
                    <div className='row'>
                        <div>Min:<input id='dribbling_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='dribbling_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>composure</div>
                    <div className='row'>
                        <div>Min:<input id='composure_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='composure_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>reactions</div>
                    <div className='row'>
                        <div>Min:<input id='reactions_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='reactions_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>balance</div>
                    <div className='row'>
                        <div>Min:<input id='balance_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='balance_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>agility</div>
                    <div className='row'>
                        <div>Min:<input id='agility_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='agility_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
            </div>
        </div>
    }
    const showDefendingFilter = () => {
        return <div className='extended-attributes-bottom'>
            <div className='group'>
                <div className="line">
                    <div className='label'>standing tackle</div>
                    <div className='row'>
                        <div>Min:<input id='standingTackle_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='standingTackle_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>sliding tackle</div>
                    <div className='row'>
                        <div>Min:<input id='slidingTackle_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='slidingTackle_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>defence awareness</div>
                    <div className='row'>
                        <div>Min:<input id='defenseAwareness_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='defenseAwareness_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>heading accuracy</div>
                    <div className='row'>
                        <div>Min:<input id='headingAccuracy_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='headingAccuracy_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>interceptions</div>
                    <div className='row'>
                        <div>Min:<input id='interceptions_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='interceptions_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
            </div>
        </div>
    }
    const showPhysicalityFilter = () => {
        return <div className='extended-attributes-bottom'>
            <div className='group'>
                <div className="line">
                    <div className='label'>jumping</div>
                    <div className='row'>
                        <div>Min:<input id='jumping_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='jumping_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>strength</div>
                    <div className='row'>
                        <div>Min:<input id='strength_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='strength_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>stamina</div>
                    <div className='row'>
                        <div>Min:<input id='stamina_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='stamina_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>aggresion</div>
                    <div className='row'>
                        <div>Min:<input id='aggression_min' type='number' min={0} max={99} /></div>
                        <div>Max:<input id='aggression_max' type='number' min={0} max={99} /></div>
                    </div>
                </div>
            </div>
        </div>
    }
    const showGoalKeeperFilter = () => {
        return <div className='extended-attributes-bottom'>
            <div className='group'>
                <div className="line">
                    <div className='label'>diving</div>
                    <div className='row'>
                        <div>Min:<input type='diving_min' min={0} max={99} /></div>
                        <div>Max:<input type='diving_max' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>handling</div>
                    <div className='row'>
                        <div>Min:<input type='handling_min' min={0} max={99} /></div>
                        <div>Max:<input type='handling_max' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>kicking</div>
                    <div className='row'>
                        <div>Min:<input type='kicking_min' min={0} max={99} /></div>
                        <div>Max:<input type='kicking_max' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>positioning</div>
                    <div className='row'>
                        <div>Min:<input type='positioning_min' min={0} max={99} /></div>
                        <div>Max:<input type='positioning_max' min={0} max={99} /></div>
                    </div>
                </div>
                <div className="line">
                    <div className='label'>reflexes</div>
                    <div className='row'>
                        <div>Min:<input type='reflexes_min' min={0} max={99} /></div>
                        <div>Max:<input type='reflexes_max' min={0} max={99} /></div>
                    </div>
                </div>
            </div>
        </div>
    }
    const searchWithFilter = () => {
        if (extendedSearch) {
            fetchPlayersExtended(0);
        }
        else {
            fetchPlayers(0);
        }

        setActivePage(0);
    }
    const getMyPlayers = () => {
        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'get',
            url: baseUrl + '/api/fifa/user/club/get-my-players/',
            headers: { 'Authorization': token },
        }).then((response) => {
            console.log("myPlayers: ", response.data.data);

            // TODO: refresh token 

            setPlayersInMyClub(response.data.data);
        }).catch((error) => {
            console.log(error);

            // navigate("/")
        })
    }

    const add_or_remove_player = (isInClub, playerId) => {
        console.log("changing my club...", isInClub);

        if (isInClub) {
            const index = playersInMyClub.findIndex(element => element.id === playerId);

            let club = [...playersInMyClub];

            club.splice(index, 1);

            setPlayersInMyClub([...club]);

            const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

            axios({
                method: 'delete',
                url: baseUrl + '/api/fifa/user/club/remove-player/' + playerId +'/',
                headers: { 'Authorization': token },
            }).then((response) => {
                // console.log("myPlayers: ", response.data.data);

                // TODO: refresh token 

                // setPlayersInMyClub(response.data.data);
            }).catch((error) => {
                console.log(error);

                // navigate("/")
            })
        }
        else {
            const index = players.findIndex(element => element.id === playerId);

            let club = [...playersInMyClub];

            club.push(players[index]);

            setPlayersInMyClub([...club]);

            const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

            axios({
                method: 'post',
                url: baseUrl + '/api/fifa/user/club/add-player/' + playerId +'/',
                headers: { 'Authorization': token },
            }).then((response) => {
                // console.log("myPlayers: ", response.data.data);

                // TODO: refresh token 

                // setPlayersInMyClub(response.data.data);
            }).catch((error) => {
                console.log(error);

                // navigate("/")
            })
        }
    }

    useEffect(() => {
        // console.log('*** DATABASE ***');
        fetchLeagues();
        fetchClubs();
        fetchNations();
        checkToken();
        getMyPlayers();
    }, [pageloaded]);

    return (<div className='database'>
        <TopMenu />
        <div className='nav-bar' onClick={showLocalState}>
            {/* <div className='nav-bar'> */}
            <div>Select your filter attributes:</div>
            <div className='filter-rows'>
                <div className='row'>
                    <label htmlFor="ligue-select">Select a Ligue: </label>
                    <select name="ligues" id="ligue-select" onChange={(e) => dropDownLeaguesChanged(e.target.value)}>
                        <option value="">{selectedleague}</option>
                        {leagues.map((item) => {
                            if (item.name !== selectedleague) {
                                return <option key={Math.random() * 100000} value={item.name}>{item.name}</option>
                            }
                        })}
                        <option value="">{'--- none ---'}</option>
                    </select>
                </div>
                <div className='row'>
                    <label htmlFor="nation-select">Select a Nationality: </label>
                    <select name="nations" id="nation-select" onChange={(e) => dropDownNationsChanged(e.target.value)}>
                        <option value="" id='nation-select-shown'>{selectedNation}</option>
                        {nations.map((item) => {
                            if (item.name !== selectedNation) {
                                return <option key={Math.random() * 100000} value={item.name}>{item.name}</option>
                            }
                        })}
                        <option value="">{'--- none ---'}</option>
                    </select>
                </div>
                <div className='row'>
                    <label htmlFor="club-select">Select a Club: </label>
                    <select name="clubs" id="club-select" onChange={(e) => dropDownClubsChanged(e.target.value)}>
                        <option value="" id='club-select-shown'>{selectedClub}</option>
                        {filteredClubs.map((item) => {
                            if (item.name !== selectedClub) {
                                return <option key={Math.random() * 100000} value={item.name}>{item.name}</option>
                            }
                        })}
                        <option value="">{'--- none ---'}</option>
                    </select>
                </div>
                <div className={extendedSearch ? 'row hidden' : 'row'}>
                    <label htmlFor="players-name">Players Name: </label>
                    <input type='text' maxLength={20} value={playersName} onChange={(e) => setPlayersName(e.target.value)} />
                </div>
            </div>
            <div className={extendedSearch ? 'extended-attributes' : 'extended-attributes-hidden'}>
                <div className={extendedFilter === 'shooting' ? 'active' : ''} onClick={() => setExtendedFilter('shooting')}>Shooting</div>
                <div className={extendedFilter === 'pace' ? 'active' : ''} onClick={() => setExtendedFilter('pace')}>Pace</div>
                <div className={extendedFilter === 'passing' ? 'active' : ''} onClick={() => setExtendedFilter('passing')}>Passing</div>
                <div className={extendedFilter === 'dribling' ? 'active' : ''} onClick={() => setExtendedFilter('dribling')}>Dribling</div>
                <div className={extendedFilter === 'defending' ? 'active' : ''} onClick={() => setExtendedFilter('defending')}>Defending</div>
                <div className={extendedFilter === 'physicality' ? 'active' : ''} onClick={() => setExtendedFilter('physicality')}>Physicality</div>
                <div className={extendedFilter === 'goalkeeper' ? 'active' : ''} onClick={() => setExtendedFilter('goalkeeper')}>Goalkeeper</div>
            </div>
            {extendedFilter === 'shooting' && extendedSearch ? showShootingFilter() : ''}
            {extendedFilter === 'pace' && extendedSearch ? showPaceFilter() : ''}
            {extendedFilter === 'passing' && extendedSearch ? showPassingFilter() : ''}
            {extendedFilter === 'dribling' && extendedSearch ? showDriblingFilter() : ''}
            {extendedFilter === 'defending' && extendedSearch ? showDefendingFilter() : ''}
            {extendedFilter === 'physicality' && extendedSearch ? showPhysicalityFilter() : ''}
            {extendedFilter === 'goalkeeper' && extendedSearch ? showGoalKeeperFilter() : ''}
            <div>
                <input type='button' value={extendedSearch ? 'HIDE EXTENDED SEARCH' : 'SHOW EXTENDED SEARCH'} onClick={() => setExtendedSearch(!extendedSearch)} className={'isClickable'} />
                <input type='button' value='FIND PLAYERS' onClick={searchWithFilter} className='isClickable' />
                <input type='button' value='DISPLAY MY CLUB' onClick={() => setPlayers([...playersInMyClub])} className={loggedUser.username == "" ? "isHidden" : 'isClickable'} />
            </div>
            <div className='info-panel'>
                <div>filtered results: {amountPlayers}</div>
                <div className='previous-page' onClick={listToPreviousPage}>previous page</div>
                <div className='page'>{activePage + 1}</div>
                <div className='next-page' onClick={listToNextPage}>next page</div>
                <div>showing {1 + (activePage) * 10} to {(activePage + 1) * 10 < amountPlayers ? (activePage + 1) * 10 : amountPlayers}</div>
            </div>
        </div>
        {players.map((item) => {
            return <PlayerInfo key={Math.random() * 100000} player={item} isInClub={playersInMyClub.findIndex(element => element.id === item.id) !== -1} action={add_or_remove_player} />;
        })}
    </div>);
}

export default Database;