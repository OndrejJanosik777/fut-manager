import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import NavBar from '../../components/nav-bar';
import { useNavigate } from 'react-router-dom';

const ModifyTeam = () => {
    const localUrl = 'http://127.0.0.1:8000';
    const herokuUrl = 'https://fut-manager-api.herokuapp.com';

    // page loaded - fake state - just to run first hook at start...
    const [loaded, setLoaded] = useState(0);
    // local state of component
    const [players, setPlayers] = useState([]);
    const [baseUrl, setBaseUrl] = useState(herokuUrl);
    // modify player
    const [newId, setNewID] = useState(-1);
    const [newName, setNewName] = useState('');
    const [newBirthday, setNewBirthday] = useState('');
    const [newPlayedGames, setNewPlayedGames] = useState(0);
    const [newScoredGoals, setNewScoredGoals] = useState(0);
    const [newPosition, setNewPosition] = useState('');
    const [value, setValue] = useState('write something')

    // hook navigate
    const navigate = useNavigate();

    // fetch all players from database
    const getAllPlayersFromDatabase = () => {
        // idea is to fetch all players from database
        // and filter one with at least one goal
        const axios = require('axios');

        const url = baseUrl + "/api/players/";

        axios.get(url)
            .then((response) => {
                // console.log('response: ', response);

                // get all players
                const allPlayers = response.data;
                // filter for players with at least 1 scored goal
                // let succesfullPlayers = allPlayers.filter(player => player.scored_goals > 0);
                // sort accorsing ratio scored_goals / player_games
                allPlayers.sort((a, b) => a.id - b.id);


                setPlayers(allPlayers);
            })
            .catch((error) => {
                console.log('error: ', error);
            })


    }

    // this hook will only run once - to load players from backend database...
    useEffect(() => {
        getAllPlayersFromDatabase();

        console.log('inside useEffect function....');
    }, [loaded]);

    const pictureStyle = (pictureUrl) => {
        // console.log('pictureUrl: ', pictureUrl);

        return {
            width: '85px',
            height: '100px',
            backgroundColor: 'white',
            backgroundImage: 'url(' + pictureUrl + ')',
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            borderRadius: '15px',
        }
    }

    const modifyPlayer = (index) => {
        console.log('modifying player with id: ', players[index].id)

        setNewID(players[index].id);
        setNewName(players[index].name);
        setNewBirthday(players[index].birthday);
        setNewPlayedGames(players[index].played_games);
        setNewScoredGoals(players[index].scored_goals);
        setNewPosition(players[index].position);
    }

    const savePlayer = (index) => {
        console.log('saving player with id: ', players[index].id)

        const axios = require('axios');

        axios({
            method: 'patch',
            url: `https://fut-manager-api.herokuapp.com/api/players/${players[index].id}/`,
            data: {
                name: newName,
                birthday: newBirthday,
                scored_goals: newScoredGoals,
                played_games: newPlayedGames,
                position: newPosition,
            }
        })
            .then(() => {
                console.log('saved sucesfully...');
            })

        setNewID(-1);

        let newPlayers = [...players];
        newPlayers[index].name = newName;
        newPlayers[index].birthday = newBirthday;
        newPlayers[index].scored_goals = newScoredGoals;
        newPlayers[index].played_games = newPlayedGames;
        newPlayers[index].position = newPosition;

        setPlayers(newPlayers);
    }

    const deletePlayer = (id) => {
        console.log('deleting player with id: ', id)
    }

    return (<div>
        <header>
            <NavBar activeField='4' />
        </header>
        <main className='w-full flex flex-col  justify-center items-center mt-8 mb-8'>
            <p className='w-full max-w-screen-xl m-2'>Note: ratio is calculated as scored goals divided by played games</p>
            <table className='w-full border-black border max-w-screen-xl'>
                <thead className='border-black border'>
                    <tr className='border-black border'>
                        <th>id</th>
                        <th>name</th>
                        <th>birthday</th>
                        <th>Games played</th>
                        <th>Goals scored</th>
                        <th>Position</th>
                        <th>Picture</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {players.map((player, index) => {

                        if (player.id != newId) {
                            return <tr key={Math.random() * 100000} className="border-black border" >
                                <td className='text-center p-2 text-xl font-bold border-black border'>{player.id}</td>
                                <td className='p-2 text-xl border-black border'>{player.name}</td>
                                <td className='text-center text-xl border-black border'>{player.birthday}</td>
                                {/* <td className='text-center text-xl border-black border'>{player.birthday}</td> */}
                                <td className='text-center text-xl border-black border'>{player.played_games}</td>
                                <td className='text-center text-xl border-black border'>{player.scored_goals}</td>
                                <td className='text-center text-xl border-black border'>{player.position}</td>
                                <td className='flex justify-center text-xl items-center'><div style={pictureStyle(player.image)}></div></td>
                                <td className='text-center text-xl border-black border'>
                                    <input type='button' value='modify' className='bg-blue-300 p-1 text-sm rounded-lg border-black border-2 cursor-pointer hover:bg-blue-400' onClick={() => modifyPlayer(index)} />
                                    {/* <input type='button' value='save' className='bg-green-300 p-1 text-sm rounded-lg border-black border-2 cursor-pointer hover:bg-green-400' onClick={() => savePlayer(index)} /> */}
                                    <input type='button' value='delete' className='bg-red-300 p-1 text-sm rounded-lg border-black border-2 cursor-pointer hover:bg-red-400' onClick={() => deletePlayer(index)} />
                                </td>
                            </tr>
                        }
                        else {
                            return <tr key={Math.random() * 100000} className="border-black border" >
                                <td className='text-center p-2 text-xl font-bold border-black border'>{player.id}</td>
                                <td className='p-2 text-xl border-black border'>
                                    <input type='text' value={newName} onChange={(e) => setNewName(e.target.value)} className='bg-gray-200 border-gray-500 border divide-solid w-max' />
                                </td>
                                <td className='p-2 text-xl border-black border'>
                                    <input type='text' value={newBirthday} onChange={(e) => setNewBirthday(e.target.value)} className='bg-gray-200 border-gray-500 border divide-solid w-28' />
                                </td>
                                <td className='p-2 text-xl border-black border'>
                                    <input type='text' value={newPlayedGames} onChange={(e) => setNewPlayedGames(e.target.value)} className='bg-gray-200 border-gray-500 border divide-solid w-14' />
                                </td>
                                <td className='p-2 text-xl border-black border'>
                                    <input type='text' value={newScoredGoals} onChange={(e) => setNewScoredGoals(e.target.value)} className='bg-gray-200 border-gray-500 border divide-solid w-14' />
                                </td>
                                <td className='p-2 text-xl border-black border'>
                                    <input type='text' value={newPosition} onChange={(e) => setNewPosition(e.target.value)} className='bg-gray-200 border-gray-500 border divide-solid w-14' />
                                </td>
                                <td className='flex justify-center text-xl items-center'><div style={pictureStyle(player.image)}></div></td>
                                <td className='text-center text-xl border-black border'>
                                    {/* <input type='button' value='modify' className='bg-blue-300 p-1 text-sm rounded-lg border-black border-2 cursor-pointer hover:bg-blue-400' onClick={() => modifyPlayer(index)} /> */}
                                    <input type='button' value='save' className='bg-green-300 p-1 text-sm rounded-lg border-black border-2 cursor-pointer hover:bg-green-400' onClick={() => savePlayer(index)} />
                                    <input type='button' value='delete' className='bg-red-300 p-1 text-sm rounded-lg border-black border-2 cursor-pointer hover:bg-red-400' onClick={() => deletePlayer(index)} />
                                </td>
                            </tr>
                        }
                    })}
                </tbody>
            </table>
        </main>
        <input type='text' value={value} onChange={(e) => setValue(e.target.value)} />
        <input type='button' onClick={() => navigate('/team/add-player')} value='add new player' className='bg-green-300 p-2 rounded-lg border-black border-2 cursor-pointer hover:bg-green-400' />
    </div>);
}

export default ModifyTeam;