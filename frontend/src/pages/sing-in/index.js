import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import LogInPicture from './assets/login.webp'
import './index.scss';

const SignIn = () => {
    const navigate = useNavigate();
    const axios = require('axios');

    const getBaseUrl = () => {
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // dev code
            return 'http://127.0.0.1:8000';
        } else {
            // production code
            return 'https://fut-v2.herokuapp.com';
        }
    }

    const [baseUrl, setBaseUrl] = useState(getBaseUrl());

    const createAccount = () => {
        console.log('creating new account');

        // axios({
        //     method: 'post',
        //     url: baseUrl + '/api/fut/users/',
        //     data: {
        //         username: username,
        //         email: email,
        //         password: password
        //     }
        // })
        //     .then((response => {
        //         alert('Account created succesfully!');
        //     }))
        //     .catch((error) => {
        //         console.log(error);
        //     })

        navigate('/')
    }

    const fetchUsers = () => {
        axios.get(baseUrl + '/api/fut/users/')
            .then((response) => {
                console.log('users: ', response.data);
                // setUsers(response.data);
            })
            .catch((error) => {
                console.log(error);
            })
    }

    useEffect(() => {
        console.log('sign in page loaded')
        // fetchUsers();
    }, []);

    return (<div className='sign-in-page'>
        <h1 className='h_1'>Choose your credentials:</h1>
        <div className='buttons-container' >
            <input className='input-field_1' type='text' id='username' name='username' placeholder='username' />
            <input className='input-field_1' type='password' id='password' name='password' placeholder='password' />
            <input className='input-field_1' type='password' id='confirm-password' name='confirm-password' placeholder='confirm password' />
            <input type='button' value='Create an account' className='button_1' onClick={() => createAccount()} />
        </div>
        <div className='picture'>
            <img src={LogInPicture} alt=''></img>
        </div>
    </div>);
}

export default SignIn;