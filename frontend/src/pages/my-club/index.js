import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import TopMenu from '../../components/top-menu';
import BatchCountPlayers from '../../components/batch-count-players';
import BatchCountPlayersNations from '../../components/batch-count-players-nations';
import BatchAttributes from '../../components/batch-attributes';
import ModalPlayers from '../../components/modal-players';
import PlayerScoreBar from '../../components/player-score-bar';
import "./index.scss"
var moment = require('moment');

const MyClub = () => {
    const axios = require('axios');
    const navigate = useNavigate();

    const getBaseUrl = () => {
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // dev code
            return 'http://127.0.0.1:8000';
        } else {
            // production code
            return 'https://fut-v2.herokuapp.com';
        }
    }

    const [pageloaded, setPageLoaded] = useState(0);
    const [baseUrl, setBaseUrl] = useState(getBaseUrl());
    const [loggedUser, setLoggedUser] = useState({ username: '' });
    const [userPlayers, setUserPlayers] = useState([]);
    const [sorted_nations, setSortedNations] = useState([{ id: 1, count: 1 }, { id: 1, count: 1 }, { id: 1, count: 1 }]);
    const [sorted_leagues, setSortedLeagues] = useState([{ id: 1, count: 1 }, { id: 1, count: 1 }, { id: 1, count: 1 }]);
    const [sorted_clubs, setSortedClubs] = useState([{ id: 1, count: 1 }, { id: 1, count: 1 }, { id: 1, count: 1 }]);
    const [bestPlayers, setBestPlayers] = useState([]);
    const [showModalPlayers, setShowModalPlayers] = useState(false);
    const [userPoints, setUserPoints] = useState([]);
    const [userFilteredPoints, setUserFilteredPoints] = useState([]);
    const [totalScore, setTotalScore] = useState(0)
    const [filteredScore, setFilteredScore] = useState(0)
    const [attributes, setAttributes] = useState("offensive")
    const [filterCompetition, setFilterCompetition] = useState("ALL");
    const [filterTimeRange, setFilterTimeRange] = useState("this week");
    const [bestOffensivePlayers, setBestOffensivePlayers] = useState([]);
    const [bestDeffensivePlayers, setBestDeffensivePlayers] = useState([]);
    const [bestMiddlePlayers, setBestMiddlePlayers] = useState([]);
    /* fetching functions - read data from backend and set it to component state */
    const checkToken = () => {
        console.log('PAGE: MY-CLUB: function checkToken');

        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'get',
            url: baseUrl + '/api/fut/user/',
            headers: { 'Authorization': token },
        }).then((response) => {
            // console.log("loggedUser: ", response.data);

            // TODO: refresh token 

            setLoggedUser(response.data);
        }).catch((error) => {
            console.log(error);
            alert('you lost you credentials');
            navigate("/");
        })
    }
    const fetchUserPoints = () => {
        console.log('PAGE: MY-CLUB: function fetchPoints');

        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'get',
            url: baseUrl + '/api/fifa/point/list/',
            headers: { 'Authorization': token },
        }).then((response) => {
            // TODO: refresh token 
            calculateScore([...response.data]);

            setUserPoints(response.data);

            // calculateTotalScore();
        }).catch((error) => {
            console.log(error);
            alert('problem with fetching points');
            // navigate("/")
        })
    }
    const fetchNations = () => {
        console.log('PAGE: MY-CLUB: function extractNations');

        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'get',
            url: baseUrl + '/api/fifa/data/sorted-nations/',
            headers: { 'Authorization': token },
        }).then((response) => {
            setSortedNations(response.data);
        }).catch((error) => {
            console.log(error);
            alert('problem with fetching nations');
            // navigate("/");
        })
    }
    const fetchSortedLeagues = () => {
        console.log('PAGE: MY-CLUB: function extractLeagues');

        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'get',
            url: baseUrl + '/api/fifa/data/sorted-leagues/',
            headers: { 'Authorization': token },
        }).then((response) => {
            // console.log('sorted leagues: ', response.data)

            setSortedLeagues(response.data);
        }).catch((error) => {
            console.log(error);
            alert('problem with fetching leagues');
            // navigate("/");
        })
    }
    const fetchSortedClubs = () => {
        console.log('PAGE: MY-CLUB: function extractClubs');

        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'get',
            url: baseUrl + '/api/fifa/data/sorted-clubs/',
            headers: { 'Authorization': token },
        }).then((response) => {
            setSortedClubs(response.data);
        }).catch((error) => {
            console.log(error);
            alert('problem with fetching clubs');
            // navigate("/");
        })
    }
    const fetchBestPlayers = () => {
        console.log('PAGE: MY-CLUB: function extractBestPlayers');

        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'get',
            url: baseUrl + '/api/fifa/data/get-best-players/',
            headers: { 'Authorization': token },
        }).then((response) => {
            setBestPlayers(response.data);
        }).catch((error) => {
            console.log(error);
            alert('problem with fetching bestPlayers');
            // navigate("/");
        })
    }
    const fetchUserPlayers = () => {
        console.log('PAGE: MY-CLUB: function getMyPlayers');

        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'get',
            url: baseUrl + '/api/fifa/user/club/get-my-players/',
            headers: { 'Authorization': token },
        }).then((response) => {
            // console.log("myPlayers: ", response.data.data);

            // TODO: refresh token 

            setUserPlayers(response.data.data);
        }).catch((error) => {
            console.log(error);
            alert('problem with fetching userPlayers');
            // navigate("/");
        })
    }
    /* secondary - needs already some input calling function according updating state */
    const calculateUserFilteredPoints = (points) => {
        console.log('PAGE: MY-CLUB: function calculateUserFilteredPoints');

        const now = moment();

        let relevantPoints = [];

        switch (filterTimeRange) {
            case "this week":
                points.map((item) => {

                    if (now.year() === moment(item.date).year() && now.week() === moment(item.date).week()) {
                        if (filterCompetition === "ALL") {
                            relevantPoints.push(item);
                        }
                        else {
                            if (filterCompetition === item.competition) relevantPoints.push(item)
                        }
                    }
                })
                break;
            case "last week":
                points.map((item) => {

                    if (now.year() === moment(item.date).year() && now.week() - 1 === moment(item.date).week() && now.week() > 0) {
                        if (filterCompetition === "ALL") {
                            relevantPoints.push(item);
                        }
                        else {
                            if (filterCompetition === item.competition) relevantPoints.push(item)
                        }
                    }

                    const weeksLastYear = now.year() - 1;

                    if (now.year() - 1 === moment(item.date).year() && moment().weeksInYear(weeksLastYear - 1) === moment(item.date).week() && now.week() === 0) {
                        if (filterCompetition === "ALL") {
                            relevantPoints.push(item);
                        }
                        else {
                            if (filterCompetition === item.competition) relevantPoints.push(item)
                        }
                    }
                })
                break;
            case "this month":
                points.map((item) => {
                    if (now.year() === moment(item.date).year() && now.month() === moment(item.date).month()) {
                        if (filterCompetition === "ALL") {
                            relevantPoints.push(item);
                        }
                        else {
                            if (filterCompetition === item.competition) relevantPoints.push(item)
                        }
                    }
                })
                break;
            case "last month":
                points.map((item) => {
                    if (now.year() === moment(item.date).year() && now.month() - 1 === moment(item.date).month() && now.month() > 0) {
                        if (filterCompetition === "ALL") {
                            relevantPoints.push(item);
                        }
                        else {
                            if (filterCompetition === item.competition) relevantPoints.push(item)
                        }
                    }

                    if (now.month() === 0 && now.year() - 1 === moment(item.date).year() && moment(item.date).month() === 11) {
                        if (filterCompetition === "ALL") {
                            relevantPoints.push(item);
                        }
                        else {
                            if (filterCompetition === item.competition) relevantPoints.push(item)
                        }
                    }
                })
                break;
            case "all":
                points.map((item) => {
                    if (filterCompetition === "ALL") {
                        relevantPoints.push(item);
                    }
                    else {
                        if (filterCompetition === item.competition) relevantPoints.push(item)
                    }
                })
                break;
        }

        // setUserFilteredPoints([...relevantPoints]);
        return [...relevantPoints];
    }
    const findBestPlayers = (points) => {
        console.log('PAGE: MY-CLUB: function findBestPlayers');

        const offensive = ['LW', 'CF', 'ST', 'RW'];
        const midfield = ['LM', 'CM', 'RM', 'CAM'];
        const defensive = ['CDM', 'LB', 'RB', 'RWB', 'LWB', 'CB'];

        let offensivePlayers = [];
        let midfieldPlayers = [];
        let defensivePlayers = [];

        userPlayers.map((item) => {
            let player = { ...item };
            let playersPoints = points.filter((elem) => elem.player === player.id)

            let playerScore = calculateScore(playersPoints);

            player['points'] = playerScore;

            if (offensive.indexOf(item.position) !== -1 && playerScore !== 0) offensivePlayers.push({ ...player });
            if (midfield.indexOf(item.position) !== -1 && playerScore !== 0) midfieldPlayers.push({ ...player });
            if (defensive.indexOf(item.position) !== -1 && playerScore !== 0) defensivePlayers.push({ ...player });
        })


        offensivePlayers.sort((a, b) => {
            return b.points - a.points
        }
        )
        midfieldPlayers.sort((a, b) => {
            return b.points - a.points
        }
        )
        defensivePlayers.sort((a, b) => {
            return b.points - a.points
        }
        )

        setBestOffensivePlayers([...offensivePlayers]);
        setBestMiddlePlayers([...midfieldPlayers]);
        setBestDeffensivePlayers([...defensivePlayers]);
    }
    const showState = () => {
        console.log('PAGE: MY-CLUB: function showState');
        console.log("userPoints: ", userPoints);
        console.log("userPlayers: ", userPlayers);
        console.log("userFilteredPoints: ", userFilteredPoints);
        console.log("bestOffensivePlayers: ", bestOffensivePlayers);
        console.log("bestDeffensivePlayers: ", bestDeffensivePlayers);
        console.log("bestMiddlePlayers: ", bestMiddlePlayers);
    }
    /* independent functions - takes input, gives output */
    const calculateScore = (points) => {
        console.log('PAGE: MY-CLUB: function calculateScore');

        let totalScore = 0;

        points.map((item) => {

            switch (item.competition) {
                case "CH":
                    totalScore += 700;
                    break;
                case "DR":
                    if (item.DR_league === "ED") totalScore += 1100;
                    if (item.DR_league === "D1") totalScore += 1000;
                    if (item.DR_league === "D2") totalScore += 900;
                    if (item.DR_league === "D3") totalScore += 800;
                    if (item.DR_league === "D4") totalScore += 700;
                    if (item.DR_league === "D5") totalScore += 600;
                    if (item.DR_league === "D6") totalScore += 500;
                    if (item.DR_league === "D7") totalScore += 400;
                    if (item.DR_league === "D8") totalScore += 300;
                    if (item.DR_league === "D9") totalScore += 200;
                    if (item.DR_league === "D10") totalScore += 100;
                    break;
                case "SB":
                    if (item.SB_difficulty === "Amateur") totalScore += 1 * item.SB_oponnent_strength;
                    if (item.SB_difficulty === "Beginner") totalScore += 3 * item.SB_oponnent_strength;
                    if (item.SB_difficulty === "Semi-Pro") totalScore += 4 * item.SB_oponnent_strength;
                    if (item.SB_difficulty === "Pro") totalScore += 5 * item.SB_oponnent_strength;
                    if (item.SB_difficulty === "World Class") totalScore += 7 * item.SB_oponnent_strength;
                    if (item.SB_difficulty === "Legend") totalScore += 9 * item.SB_oponnent_strength;
                    if (item.SB_difficulty === "Ultimate") totalScore += 11 * item.SB_oponnent_strength;
                    break;
            }
        })

        // setTotalScore(totalScore);

        return totalScore;
    }
    useEffect(() => {
        console.log('PAGE: MY-CLUB: Mounting');

        checkToken();
        fetchUserPlayers();
        fetchNations();
        fetchSortedLeagues();
        fetchSortedClubs();
        fetchBestPlayers();
        fetchUserPoints();
    }, [pageloaded]);
    useEffect(() => {
        // function will trigger when state userPoints will update...
        console.log('PAGE: MY-CLUB: userPoints state is updated');

        let score = calculateScore([...userPoints]);
        setTotalScore(score);

        let points = calculateUserFilteredPoints([...userPoints]);
        setUserFilteredPoints([...points]);
        findBestPlayers([...points]);
    }, [userPoints])
    useEffect(() => {
        // function will trigger when state userPoints will update...
        console.log('PAGE: MY-CLUB: userPlayers state is updated');

        let points = calculateUserFilteredPoints([...userPoints]);

        findBestPlayers([...points]);
    }, [userPlayers])
    useEffect(() => {
        // function will trigger when state userPoints will update...
        console.log('PAGE: MY-CLUB: userPoints state is updated');

        let score = calculateScore([...userFilteredPoints]);
        setFilteredScore(score);
        // calculateUserFilteredPoints([...userPoints]);

        findBestPlayers([...userFilteredPoints]);
    }, [userFilteredPoints])
    useEffect(() => {
        // function will trigger when state filterCompetition or filterTimeRange will update...
        console.log('PAGE: MY-CLUB: filterCompetition or filterTimeRange state is updated');

        let points = calculateUserFilteredPoints([...userPoints]);
        findBestPlayers([...points]);

        setUserFilteredPoints([...points]);

        console.log('[filterCompetition, filterTimeRange] changed')
    }, [filterCompetition, filterTimeRange])

    return (<div>
        {showModalPlayers ? <ModalPlayers players={userPlayers} close={() => setShowModalPlayers(false)} /> : ""}
        <TopMenu />
        <div className='my-club'>
            <div className='middle-container'>
                <div className='faked-opacity'></div>
                <div className='used-area'>
                    <div className='upper-info'>
                        <div onClick={showState}>NUMBER OF PLAYERS: <strong>{userPlayers.length}</strong></div>
                        <div>TOTAL SCORE: <strong>{totalScore}</strong></div>
                    </div>
                    <div className='see-all' onClick={() => setShowModalPlayers(true)}>see all...</div>
                    <div className='leagues-clubs-nations'>
                        <div className='group'>
                            <div>{sorted_leagues.length > 0 ? < BatchCountPlayers amount={sorted_leagues[0].count} imageUrl={baseUrl + "/api/fifa/league/image/" + sorted_leagues[0].id} scheme='dark' /> : ""}</div>
                            <div>{sorted_leagues.length > 1 ? < BatchCountPlayers amount={sorted_leagues[1].count} imageUrl={baseUrl + "/api/fifa/league/image/" + sorted_leagues[1].id} scheme='dark' /> : ""}</div>
                            <div>{sorted_leagues.length > 2 ? < BatchCountPlayers amount={sorted_leagues[2].count} imageUrl={baseUrl + "/api/fifa/league/image/" + sorted_leagues[2].id} scheme='dark' /> : ""}</div>
                        </div>
                        <div className='group'>
                            <div>{sorted_clubs.length > 0 ? < BatchCountPlayers amount={sorted_clubs[0].count} imageUrl={baseUrl + "/api/fifa/club/image/" + sorted_clubs[0].id} scheme='transparent' /> : ""}</div>
                            <div>{sorted_clubs.length > 1 ? < BatchCountPlayers amount={sorted_clubs[1].count} imageUrl={baseUrl + "/api/fifa/club/image/" + sorted_clubs[1].id} scheme='transparent' /> : ""}</div>
                            <div>{sorted_clubs.length > 2 ? < BatchCountPlayers amount={sorted_clubs[2].count} imageUrl={baseUrl + "/api/fifa/club/image/" + sorted_clubs[2].id} scheme='transparent' /> : ""}</div>
                        </div>
                        <div className='group'>
                            <div>{sorted_nations.length > 0 ? < BatchCountPlayersNations amount={sorted_nations[0].count} imageUrl={baseUrl + "/api/fifa/nation/image/" + sorted_nations[0].id} /> : ""}</div>
                            <div>{sorted_nations.length > 1 ? < BatchCountPlayersNations amount={sorted_nations[1].count} imageUrl={baseUrl + "/api/fifa/nation/image/" + sorted_nations[1].id} /> : ""}</div>
                            <div>{sorted_nations.length > 2 ? < BatchCountPlayersNations amount={sorted_nations[2].count} imageUrl={baseUrl + "/api/fifa/nation/image/" + sorted_nations[2].id} /> : ""}</div>
                        </div>
                    </div>
                    <div className='see-all'>see all...</div>
                    <div className='players-overview'>
                        <div>PLAYERS WITH BEST ATTRIBUTES</div>
                        <div className='small'>
                            <div className='active'>OFFENSIVE</div>
                            <div> / </div>
                            <div className=''>DEFENSIVE</div>
                        </div>
                        <div></div>
                    </div>
                    <div className='attributes'>
                        <div className='column'>
                            {[0, 1, 2].map((item) => {
                                if (bestPlayers.length > item)
                                    return < BatchAttributes
                                        key={Math.random() * 100000}
                                        scheme="dark"
                                        type={bestPlayers[item].attribute}
                                        rank={bestPlayers[item].value}
                                        batch=""
                                        firstName={bestPlayers[item].firstName}
                                        lastName={bestPlayers[item].lastName}
                                        imageUrl={baseUrl + "/api/fifa/league/image/" + bestPlayers[item].league_id}
                                    />
                            })}
                        </div>
                        <div className='column'>
                            {[3, 4, 5].map((item) => {
                                if (bestPlayers.length > item)
                                    return < BatchAttributes
                                        key={Math.random() * 100000}
                                        scheme="dark"
                                        type={bestPlayers[item].attribute}
                                        rank={bestPlayers[item].value}
                                        batch=""
                                        firstName={bestPlayers[item].firstName}
                                        lastName={bestPlayers[item].lastName}
                                        imageUrl={baseUrl + "/api/fifa/league/image/" + bestPlayers[item].league_id}
                                    />
                            })}
                        </div>
                        <div className='column'>
                            {[6, 7, 8].map((item) => {
                                if (bestPlayers.length > item)
                                    return < BatchAttributes
                                        key={Math.random() * 100000}
                                        scheme="dark"
                                        type={bestPlayers[item].attribute}
                                        rank={bestPlayers[item].value}
                                        batch=""
                                        firstName={bestPlayers[item].firstName}
                                        lastName={bestPlayers[item].lastName}
                                        imageUrl={baseUrl + "/api/fifa/league/image/" + bestPlayers[item].league_id}
                                    />
                            })}
                        </div>
                    </div>
                    <div className='see-all'>see all...</div>
                    <div className='statistics-overview'>
                        <div>ACHIEVEMENTS</div>
                        <div className='small'>
                            <div className={filterCompetition === "ALL" ? 'active switch' : 'switch'} onClick={() => setFilterCompetition("ALL")}>ALL</div>
                            <div className='spacer'> / </div>
                            <div className={filterCompetition === "DR" ? 'active switch' : 'switch'} onClick={() => setFilterCompetition("DR")}>DIVISION RIVALS</div>
                            <div className='spacer'> / </div>
                            <div className={filterCompetition === "SB" ? 'active switch' : 'switch'} onClick={() => setFilterCompetition("SB")}>SQUAD BATTLES</div>
                            <div className='spacer'> / </div>
                            <div className={filterCompetition === "CH" ? 'active switch' : 'switch'} onClick={() => setFilterCompetition("CH")}>CHAMPIONS</div>
                        </div>
                        <div className='small'>
                            <div className={filterTimeRange === "this week" ? 'switch active' : 'switch'} onClick={() => setFilterTimeRange("this week")}>THIS WEEK</div>
                            <div className='spacer'> / </div>
                            <div className={filterTimeRange === "last week" ? 'switch active' : 'switch'} onClick={() => setFilterTimeRange("last week")}>LAST WEEK</div>
                            <div className='spacer'> / </div>
                            <div className={filterTimeRange === "this month" ? 'switch active' : 'switch'} onClick={() => setFilterTimeRange("this month")}>THIS MONTH</div>
                            <div className='spacer'> / </div>
                            <div className={filterTimeRange === "last month" ? 'switch active' : 'switch'} onClick={() => setFilterTimeRange("last month")}>LAST MONTH</div>
                            <div className='spacer'> / </div>
                            <div className={filterTimeRange === "all" ? 'switch active' : 'switch'} onClick={() => setFilterTimeRange("all")}>ALL</div>
                        </div>
                    </div>
                    <div className='data'>
                        <div>SCORE: <strong>{filteredScore}</strong></div>
                        {/* <div className='row'>
                            <div>BEST SCORING DAY: <strong>{"6.1.2023 - Monday"}</strong></div>
                            <div>POINTS EARNED: <strong>{7000}</strong></div>
                        </div> */}
                        <div className='see-all'>see all...</div>
                        {/* <div>BEST PLAYERS: <strong>THIS WEEK</strong></div> */}
                        <div className='row'>
                            <div className='container'>
                                <div className='header-bar'>
                                    <div className='type'>OFFENSIVE PLAYERS</div>
                                    <div className='score'>G</div>
                                    <div className='score'>A</div>
                                    <div className='points'>Points</div>
                                </div>
                                {bestOffensivePlayers.map((item, index) => {
                                    if (index < 3) {
                                        let points = userPoints.filter(elem => elem.player === item.id);

                                        return <PlayerScoreBar
                                            key={Math.random() * 100000}
                                            name={item.name}
                                            position={item.position}
                                            imageUrl={baseUrl + "/api/fifa/player/avatar/" + item.id}
                                            goals={points.filter(elem => elem.type === "GOAL").length}
                                            assistances={points.filter(elem => elem.type === "ASSIST").length}
                                            points={item.points}
                                        />
                                    }
                                })}
                            </div>
                            <div className='container'>
                                <div className='header-bar'>
                                    <div className='type'>MIDFIELD PLAYERS</div>
                                    <div className='score'>G</div>
                                    <div className='score'>A</div>
                                    <div className='points'>Points</div>
                                </div>
                                {bestMiddlePlayers.map((item, index) => {
                                    if (index < 3) {
                                        let points = userPoints.filter(elem => elem.player === item.id);

                                        return <PlayerScoreBar
                                            key={Math.random() * 100000}
                                            name={item.name}
                                            position={item.position}
                                            imageUrl={baseUrl + "/api/fifa/player/avatar/" + item.id}
                                            goals={points.filter(elem => elem.type === "GOAL").length}
                                            assistances={points.filter(elem => elem.type === "ASSIST").length}
                                            points={item.points}
                                        />
                                    }
                                })}
                            </div>
                            <div className='container'>
                                <div className='header-bar'>
                                    <div className='type'>DEFENSIVE - PLAYERS</div>
                                    <div className='score'>G</div>
                                    <div className='score'>A</div>
                                    <div className='points'>Points</div>
                                </div>
                                {bestDeffensivePlayers.map((item, index) => {
                                    if (index < 3) {
                                        let points = userPoints.filter(elem => elem.player === item.id);

                                        return <PlayerScoreBar
                                            key={Math.random() * 100000}
                                            name={item.name}
                                            position={item.position}
                                            imageUrl={baseUrl + "/api/fifa/player/avatar/" + item.id}
                                            goals={points.filter(elem => elem.type === "GOAL").length}
                                            assistances={points.filter(elem => elem.type === "ASSIST").length}
                                            points={item.points}
                                        />
                                    }
                                })}
                            </div>
                        </div>
                    </div>
                    <div className='see-all'>see all...</div>
                </div>
            </div>
        </div>
    </div>);
}

export default MyClub;