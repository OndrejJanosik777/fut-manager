import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import LogInPicture from './assets/login.webp'
import './index.scss';
// purpose of page:
// allow user to log in
// get username, password,
// set token, firstname, lastname, username to localStorage

const LogIn = () => {
    const navigate = useNavigate();
    const axios = require('axios');

    const getBaseUrl = () => {
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // dev code
            return 'http://127.0.0.1:8000';
        } else {
            // production code
            return 'https://fut-v3.herokuapp.com';
        }
    }

    const [username, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [baseUrl, setBaseUrl] = useState(getBaseUrl());

    // let baseUrl = 'http://127.0.0.1:8000';

    useEffect(() => {
        // console.log('page login loaded...')
    }, []);

    const login = () => {
        // console.log('logging in...')

        let username = document.getElementById('username').value;
        let password = document.getElementById('password').value;

        // console.log('username: ', username);
        // console.log('password: ', password);

        axios({
            method: 'post',
            url: baseUrl + '/api/token/',
            data: {
                username: username,
                password: password
            }
        })
            .then((response => {
                // console.log('logged succesfully');
                console.log('access: ', response.data.access);
                console.log('refresh: ', response.data.refresh);

                localStorage.setItem('FUT_user_token_access', response.data.access);
                localStorage.setItem('FUT_user_token_refresh', response.data.refresh);

                navigate('/dashboard');

                alert('logged in succesfully');
            }))
            .catch((error) => {
                console.log(error);

                alert('problem with login...');
            })
    }

    return (<div className='login-page'>
        <h1 className='h_1'>Enter your credentials:</h1>
        <div className='buttons-container' >
            <input className='input-field_1' type='text' id='username' name='username' placeholder='username' />
            <input className='input-field_1' type='password' id='password' name='password' placeholder='password' />
            <input type='button' value='Log-in' className='button_1' onClick={login} />
        </div>
        <div className='picture'>
            <img src={LogInPicture} alt=''></img>
        </div>
    </div>);
}

export default LogIn;