import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import moment from 'moment';
import addImage from './assets/add.webp';
import addUser from './assets/user-add.webp';
import NavBar from '../../components/nav-bar';
import Button01 from '../../components/button_01';
import './index.scss';

const Dashboard = () => {
    const axios = require('axios');
    const navigate = useNavigate();

    const getBaseUrl = () => {
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // dev code
            return 'http://127.0.0.1:8000';
        } else {
            // production code
            return 'https://fut-v3.herokuapp.com';
        }
    }

    const [baseUrl, setBaseUrl] = useState(getBaseUrl());
    const [token, setToken] = useState("Bearer " + localStorage.getItem('FUT_user_token_access'));
    const [token_refresh, setTokenRefresh] = useState(localStorage.getItem('FUT_user_token_refresh'));
    const [user, setUser] = useState({
        username: "",
        first_name: "",
        last_name: "",
        date_joined: "",
        last_login: "",
    })

    useEffect(() => {
        console.log('page dashboard loaded...');

        checkToken();

        getUserData();
    }, []);

    const checkToken = () => {
        console.log('function refresh token ...');

        axios({
            method: 'post',
            url: baseUrl + '/api/token/refresh/',
            data: {
                refresh: token_refresh
            }
        })
            .then((response => {
                console.log('token refreshed succesfully');

                localStorage.setItem('FUT_user_token_access', response.data.access);

                setToken("Bearer " + response.data.access);

                // navigate('/dashboard');

                // alert('logged in succesfully');
            }))
            .catch((error) => {
                console.log(error);

                alert('function checktoken: not valid credentials...');

                navigate('/');
            })
    }

    const getUserData = () => {
        console.log('function getUserData  ...');

        axios({
            method: 'get',
            url: baseUrl + '/api/fifa/me',
            headers: {
                "Authorization": token
            }
        })
            .then((response => {
                console.log('user info got succesfully succesfully');

                console.log("user: ", response.data);

                setUser(response.data);
            }))
            .catch((error) => {
                console.log(error);

                alert('function getUserData: not valid credentials...');

                navigate('/');
            })
    }

    return (<div className='dashboard'>
        {/* <nav className='navbar'>
            <ul className='items'>
                <li className='item'>{moment().format('LLLL')}</li>
                <li className='item'>{user.username}</li>
            </ul>
        </nav> */}
        <NavBar user={user} />
        <main className='main'>
            <header className='header'>
                <div className='label'>Teams</div>
                <div className='action'>
                    <img className='image' src={addImage} alt='' />
                    <div>Add Team</div>
                </div>
            </header>
            <table className='data'>
                <th className='row'>
                    <td className='player-name'>name</td>
                    <td className='player-goals'>goals</td>
                    <td className='player-score'>score</td>
                </th>
                <tr className='row'>
                    <td className='player-name'>team 1</td>
                    <td className='player-goals'>10</td>
                    <td className='player-score'>15</td>
                </tr>
                <tr className='row'>
                    <td className='player-name'>team 2</td>
                    <td className='player-goals'>5</td>
                    <td className='player-score'>5</td>
                </tr>
                <tr className='row'>
                    <td className='player-name'>team 3</td>
                    <td className='player-goals'>5</td>
                    <td className='player-score'>5</td>
                </tr>
                <div className='last-row'>see all...</div>
            </table>
            <header className='header'>
                <div className='label'>Players</div>
                <div className='action'>
                    <img className='image' src={addUser} alt='' />
                    <div>Add Player</div>
                </div>
            </header>
            <table className='data'>
                <th className='row'>
                    <td className='player-name'>name</td>
                    <td className='player-goals'>goals</td>
                    <td className='player-score'>score</td>
                </th>
                <tr className='row'>
                    <td className='player-name'>player 1</td>
                    <td className='player-goals'>10</td>
                    <td className='player-score'>15</td>
                </tr>
                <tr className='row'>
                    <td className='player-name'>player 2</td>
                    <td className='player-goals'>5</td>
                    <td className='player-score'>5</td>
                </tr>
                <tr className='row'>
                    <td className='player-name'>player 3</td>
                    <td className='player-goals'>5</td>
                    <td className='player-score'>5</td>
                </tr>
                <div className='last-row'>see all...</div>
            </table>
        </main>
        <div className='buttons-container' >
            <input type='button' value='Manage Teams' className='button_1' onClick={() => navigate('/manage-teams')} />
            <input type='button' value='Add New Goals' className='button_1' onClick={() => navigate('/log-in')} />
            <input type='button' value='Delete Account' className='button_2' onClick={() => navigate('/log-in')} />
        </div>
    </div>);
}

export default Dashboard;