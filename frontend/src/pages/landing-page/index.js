import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import TopMenu from "../../components/top-menu/index.js"
import controller from './assets/PS_controller.webp';
// import '../index.scss';
import './index.scss';

const LandingPage = () => {
    const axios = require('axios');
    const navigate = useNavigate();

    const getBaseUrl = () => {
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // dev code
            return 'http://127.0.0.1:8000';
        } else {
            // production code
            return 'https://fut-v2.herokuapp.com';
        }
    }

    const [users, setUsers] = useState([]);
    const [teams, setTeams] = useState([]);
    const [allPoints, setAllPoints] = useState([]);
    const [allActivePlayers, setAllActivePlayers] = useState([]);
    const [baseUrl, setBaseUrl] = useState(getBaseUrl());

    const fetchUsers = () => {
        axios.get(baseUrl + '/api/fut/users/')
            .then((response) => {
                setUsers(response.data);
            })
            .catch((error) => {
                alert(error);
            })
    }
    const fetchPoints = () => {
        axios.get(baseUrl + '/api/fifa/point/list-all/')
            .then((response) => {
                setAllPoints(response.data);
            }).catch((error) => {
                alert(error);
            });
    }
    const fetchActivePlayers = (_teams, _goals) => {
        axios.get(baseUrl + '/api/fifa/all-active-players')
            .then((response) => {
                setAllActivePlayers(response.data);
            })
            .catch((error) => {
                alert(error);
            })
    }
    const click_Teams = (item) => {
        navigate('/team-overview/' + item.owner.id);
    }
    const showState = () => {
        console.log('users: ', users);
        console.log('allPoints: ', allPoints);
        console.log('allActivePlayers: ', allActivePlayers);
    }
    const getNumberOfPlayers = (userId) => {
        const contains = (player) => {
            let owners = [...player.user];

            return player.user.indexOf(userId) !== -1;
        }

        let players = allActivePlayers.filter(contains);

        return players.length;
    }
    const getScore = (userId) => {
        let userPoints = allPoints.filter(elem => elem.user === userId);
        let totalScore = 0;

        userPoints.map((item) => {
            console.log('item: ', item);

            switch (item.competition) {
                case "CH":
                    totalScore += 700;
                    break;
                case "DR":
                    if (item.DR_league === "ED") totalScore += 1100;
                    if (item.DR_league === "D1") totalScore += 1000;
                    if (item.DR_league === "D2") totalScore += 900;
                    if (item.DR_league === "D3") totalScore += 800;
                    if (item.DR_league === "D4") totalScore += 700;
                    if (item.DR_league === "D5") totalScore += 600;
                    if (item.DR_league === "D6") totalScore += 500;
                    if (item.DR_league === "D7") totalScore += 400;
                    if (item.DR_league === "D8") totalScore += 300;
                    if (item.DR_league === "D9") totalScore += 200;
                    if (item.DR_league === "D10") totalScore += 100;
                    break;
                case "SB":
                    if (item.SB_difficulty === "Amateur") totalScore += 1 * item.SB_oponnent_strength;
                    if (item.SB_difficulty === "Beginner") totalScore += 3 * item.SB_oponnent_strength;
                    if (item.SB_difficulty === "Semi-Pro") totalScore += 4 * item.SB_oponnent_strength;
                    if (item.SB_difficulty === "Pro") totalScore += 5 * item.SB_oponnent_strength;
                    if (item.SB_difficulty === "World Class") totalScore += 7 * item.SB_oponnent_strength;
                    if (item.SB_difficulty === "Legend") totalScore += 9 * item.SB_oponnent_strength;
                    if (item.SB_difficulty === "Ultimate") totalScore += 11 * item.SB_oponnent_strength;
                    break;
            }
        })


        return totalScore;
    }
    useEffect(() => {
        console.log('langing page is loaded');
        // fetchUsers();
        // fetchPoints();
        // fetchActivePlayers();
    }, []);

    return (<div className='landing-page'>
        <h1 className='h_1'>Welcome to FIFA manager</h1>
        <div className='buttons-container' >
            <input type='button' value='Login' className='button_1' onClick={() => navigate('/log-in')} />
            <input type='button' value='See statistics' className='button_1' />
            <input type='button' value="Don't have an accout? Create one." className='button_2' onClick={() => navigate('/sign-in')} />
        </div>
        {/* <div style={{ margin: 30 }}></div> */}
        <div className='picture'>
            <img src={controller} alt=''></img>
        </div>
    </div >);
}

export default LandingPage;