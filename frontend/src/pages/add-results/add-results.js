import React, { Component } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import plus from '../../assets/svg/plus.svg';
import remove from '../../assets/svg/remove.svg';
import NavBar from '../../components/nav-bar';
import BatchScore from '../../components/batch-score/batchScore';

const AddResults = () => {
    const localUrl = 'http://127.0.0.1:8000';
    const herokuUrl = 'https://fut-manager-api.herokuapp.com';

    // hook navigate
    const navigate = useNavigate();

    // page loaded - fake state - just to run first hook at start...
    const [loaded, setLoaded] = useState(0);
    // local state of component
    const [players, setPlayers] = useState([]);
    const [baseUrl, setBaseUrl] = useState(herokuUrl);
    const [games, setGames] = useState(0);

    // fetch all players from database
    const getAllPlayersFromDatabase = () => {
        const axios = require('axios');

        const url = baseUrl + "/api/players/";

        axios.get(url)
            .then((response) => {
                console.log('response: ', response);

                let allPlayers = response.data;
                let activePlayers = allPlayers.filter(player => player.position != 'BA')

                activePlayers.map((player) => {
                    player.delta_score = 0;
                })

                setPlayers(activePlayers);
            })
            .catch((error) => {
                console.log('error: ', error);
            })
    }

    // hooks for component did mount
    // this hook will only run once - to load players from backend database...
    useEffect(() => {
        getAllPlayersFromDatabase();
    }, [loaded])

    // some functions
    const addScore = () => {
        setGames(games + 1);
        // props.scoreUpdate(props.player, 1);

        players.map((player) => {
            player.played_games = player.played_games + 1;
        });
    }

    const removeScore = () => {
        if (games > 0) {
            setGames(games - 1);
            // props.scoreUpdate(props.player, -1);

            players.map((player) => {
                player.played_games = player.played_games - 1;
            });
        }
    }

    const btnSave_Click = () => {
        console.log('button save click...');

        players.map((player) => {
            sendUpdate(player);
        })

        navigate('/team/statistics')
    }

    const updateScore = (index, delta) => {
        // console.log('updating the score of player nr: ', index);

        players[index].delta_score = players[index].delta_score + delta;
    }

    const sendUpdate = (player) => {
        const axios = require('axios');

        axios({
            method: 'patch',
            url: `https://fut-manager-api.herokuapp.com/api/players/${player.id}/`,
            data: {
                scored_goals: player.scored_goals + player.delta_score,
                played_games: player.played_games
            }
        })
            .then(() => {
                console.log('saved sucesfully...');
            })

    }

    return (<div className='w-full h-screen bg-black'>
        <header className='w-full absolute z-10'>
            <NavBar activeField='3' />
        </header>
        <main className='w-full h-full relative pt-28'>
            <div className='flex '>
                {players.map((player, index) => {
                    return <div key={Math.random() * 100000}>
                        <BatchScore key={Math.random() * 100000} player={player} updateScore={updateScore} index={index} />
                    </div>
                })}
            </div>
            <div className='top-24 w-48 flex justify-center mt-2'>
                <div>
                    <div className='flex justify-center w-full text-2xl font-bold text-gray-50'>Games Played:</div>
                    <div className='p-1 w-full flex justify-between items-center'>
                        <div className='w-9 h-9 cursor-pointer' onClick={removeScore}><img src={remove} alt='' /></div>
                        <div className='text-4xl font-bold pb-1 text-gray-50'>{games}</div>
                        <div className='w-9 h-9 cursor-pointer' onClick={addScore} ><img src={plus} alt='' /></div>
                    </div>
                    <div className='w-full box-border'>
                        <input type='button' value='save'
                            className='bg-green-300 p-2 mt-2 rounded-lg border-gray-200 border-2 cursor-pointer hover:bg-green-400 w-full box-border'
                            onClick={btnSave_Click}
                        />
                    </div>
                </div>
            </div>
        </main>
        <div className='temporary'>
            <div onClick={() => console.log('Hello World..')}>Local state</div>
            <div onClick={() => console.log('players: ', players[10])} >players</div>
        </div>
    </div>);
}

export default AddResults;