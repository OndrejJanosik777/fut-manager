import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import reportWebVitals from './reportWebVitals';
import AddResults from './pages/add-results/add-results';
import AddGoals from './pages/add-goals';
import CreatePlayer from './pages/create-player/createPlayer';
import Database from './pages/database';
import LandingPage from './pages/landing-page';
import ManageTeams from './pages/manage-teams';
import ManageTeam from './pages/manage-team';
import LogIn from './pages/log-in';
import ModifyTeam from './pages/modify-team/modify-team';
import SignIn from './pages/sing-in';
import Statistics from './pages/statistics/statistics';
import Team from './pages/team/team';
import TeamOverview from './pages/team-overview';
import MyClub from './pages/my-club';
import Dashboard from './pages/dashboard';
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import './index.css';

import TopMenu from './components/top-menu';
import BatchCountPlayers from './components/batch-count-players';
import BatchAttributes from './components/batch-attributes';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <Routes>
      {/* open urls */}
      <Route path="/" element={< LandingPage />} />                             {/* updated */}
      <Route path='/database/' element={<Database />} />                        {/* updated */}
      <Route path='/my-club/' element={<MyClub />} />
      <Route path="/sign-in" element={< SignIn />} />                           {/* updated */}
      <Route path="/log-in" element={< LogIn />} />
      <Route path="/dashboard" element={< Dashboard />} />                      {/* updated */}
      <Route path="/team-overview/:teamId" element={< TeamOverview />} />
      <Route path="/team/create-new-player/" element={< CreatePlayer />} />
      <Route path="/manage-teams/" element={< ManageTeams />} />                {/* updated */}
      <Route path="/manage-team/" element={< ManageTeam />} />
      <Route path="/fut/add-goals/" element={< AddGoals />} />
      {/* TEMPORARY FOR COMPONENTS */}
    </Routes>
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
