import React, { Component } from 'react'
import { useState, useEffect } from 'react';
import leftArrow from './assets/left-arrow-svgrepo-com.svg';
import rightArrow from './assets/right-arrow-svgrepo-com.svg';
import PlayerInfo from '../../components/player-info';
import './index.scss';

const ModalSearchPlayers = (props) => {
    const axios = require('axios');

    const getBaseUrl = () => {
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // dev code
            return 'http://127.0.0.1:8000';
        } else {
            // production code
            return 'https://fut-v3.herokuapp.com';
        }
    }

    const [baseUrl, setBaseUrl] = useState(getBaseUrl());
    const [token, setToken] = useState("Bearer " + localStorage.getItem('FUT_user_token_access'));
    const [spin_deletingTeam, setSpin_deletingTeam] = useState(false);
    const [fetchedData, set_fetchedData] = useState({
        count: 0,
        next: null,
        previous: null,
        results: [],
    });

    useEffect(() => {
        fetchPlayers(undefined);
    }, []);

    const deleteTeam = () => {
        setSpin_deletingTeam(true);

        props.actionYes();
    }

    const fetchPlayers = (url) => {
        if (url === undefined) {
            url = baseUrl + '/api/fifa/players/'
        }

        axios({
            method: 'get',
            url: url,
            headers: { 'Authorization': token },
        }).then((response) => {
            console.log('fetched data: ', response.data);

            set_fetchedData(response.data);
        }).catch((error) => {
            console.log(error);
        })
    }

    const showState = () => {
        console.log('showing state: ');
        console.log('fetchedData: ', fetchedData);
    }

    return ( <div className='search-players'>
        <div className='background'></div>
        <div className='control-bar'>
            { fetchedData.previous == null ?
              <img className='img-arrow-hidden' src={leftArrow} alt='' /> :
              <img className='img-arrow' src={leftArrow} alt='' onClick={() => fetchPlayers(fetchedData.previous)} /> }
            <div className='message' onClick={showState}>Players {fetchedData.count}</div>
            { fetchedData.next == null ?
              <img className='img-arrow-hidden' src={rightArrow} alt='' /> :
              <img className='img-arrow' src={rightArrow} alt='' onClick={() => fetchPlayers(fetchedData.next)} /> }
        </div>
        <div className='main'>
            {fetchedData.results.map((player) => {
                //props.players // array of players in team
                let index = props.players.find((element) => element.id === player.id);

                return <PlayerInfo 
                    key={Math.random() * 100000} 
                    player={player} 
                    isActive={index} 
                    modifyPlayerInTeam={props.modifyPlayerInTeam}
                />
            })}
            <div className='buttons'>
                <button type="button" className="btn btn-primary" onClick={props.actionNo}>Cancel</button>
                { spin_deletingTeam ?
                    <div className="spinner-border" role="status"></div> :
                    <button type="button" className="btn btn-danger" onClick={deleteTeam}>Add</button>
                }
            </div>
        </div>
        <div className='control-bar-bottom'>
            <button type="button" className="btn btn-primary btn-sm" onClick={props.close}>Close</button>
        </div>
    </div> );
}
 
export default ModalSearchPlayers;