import React, { Component } from 'react'
import { useState } from 'react';
import './index.scss';

const Modal_Delete = (props) => {
    const [spin_deletingTeam, setSpin_deletingTeam] = useState(false);

    const deleteTeam = () => {
        setSpin_deletingTeam(true);

        props.actionYes();
    }

    return ( <div className='modal-delete'>
        <div className='background'></div>
        <div className='main'>
            <div className='message'>{props.text}</div>
            <div className='buttons'>
                <button type="button" className="btn btn-primary" onClick={props.actionNo}>Cancel</button>
                { spin_deletingTeam ?
                    <div class="spinner-border" role="status"></div> :
                    <button type="button" className="btn btn-danger" onClick={deleteTeam}>Delete</button>
                }
            </div>
        </div>
    </div> );
}
 
export default Modal_Delete;