import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import plus from '../../assets/svg/plus.svg';
import remove from '../../assets/svg/remove.svg';

const BatchScore = (props) => {
    // page loaded - fake state - just to run first hook at start...
    const [loaded, setLoaded] = useState(0);
    // local state of component
    const [score, setScore] = useState(0);
    const [baseUrl, setBaseUrl] = useState('http://localhost:8000');

    // local state
    const [ctx, setGtx] = useState(null);

    // hooks for component did mount
    // this hook will only run once - to load players from backend database...
    useEffect(() => {

    }, [loaded])

    // some functions
    const addScore = () => {
        setScore(score + 1);

        props.updateScore(props.index, 1);
    }

    const removeScore = () => {
        if (props.player.delta_score > 0) {
            setScore(score - 1);

            props.updateScore(props.index, -1);
        }
    }

    const eraseScore = () => {
        console.log('calling function eraseScore');
    }

    // object with css properties
    const pictureStyle = (pictureUrl) => {
        return {
            width: '94px',
            height: '123px',
            backgroundColor: 'blue',
            backgroundImage: 'url(' + pictureUrl + ')',
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            top: '20px',
        }
    }

    const noPictureStyle = () => {
        return {
            width: '94px',
            height: '123px',
            backgroundColor: 'blue',
            // backgroundImage: 'url(' + pictureUrl + ')',
            backgroundPosition: 'center',
            backgroundSize: 'cover',
        }
    }

    return (<div className='w-40 h-56 bg-gray-200 opacity-80 rounded-3xl border-black border-solid border-2 flex flex-col justify-between items-center top-1.5'>
        <div className='p-1 text-lg font-semibold'>{props.player == undefined ? 'no player' : props.player.name}</div>
        <div style={props.player == undefined ? noPictureStyle() : pictureStyle(props.player.image)}></div>
        <div className='p-1 w-32 flex justify-between items-center'>
            <div className='w-9 h-9 cursor-pointer' onClick={removeScore}><img src={remove} alt='' /></div>
            <div className='text-4xl font-bold pb-1'>{props.player.delta_score}</div>
            <div className='w-9 h-9 cursor-pointer' onClick={addScore} ><img src={plus} alt='' /></div>
        </div>
    </div>);
}

export default BatchScore;