import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import "./index.scss"


const ModalPlayers = (props) => {

    const [selectedPlayers, setSelectedPlayers] = useState([...props.selectedPlayers]);

    const addPlayer = (player) => {
        let new_players = [...selectedPlayers, player];

        setSelectedPlayers(new_players);
    }

    const removePlayer = (player) => {
        let index = selectedPlayers.findIndex(elem => elem.id === player.id);

        let newPlayers = [...selectedPlayers];

        newPlayers.splice(index, 1);

        setSelectedPlayers([...newPlayers]);
    }

    return (<div className='modal-players'>
        <div className='close' ><strong className='sign' onClick={() => props.close(selectedPlayers)}>[X]</strong></div>
        {props.players.map((item) => {
            const label = `${item.name} - ${item.rating} (${item.rarity.name})`
            if (selectedPlayers.find((elem) => elem.id === item.id) !== undefined) {
                return <div className='label-marked' onClick={() => removePlayer(item)}>{label}</div>;
            }

            return <div className='label' onClick={() => addPlayer(item)}>{label}</div>;
        })}
        {props.players.length === 0 ? <div>{"No players in cour Club."}</div> : <div></div>}
    </div>);
}

export default ModalPlayers;