import React, { Component } from 'react';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import moment from 'moment';
import './index.scss';

const NavBar = (props) => {
    // navigate hook
    const navigate = useNavigate();

    return (<div className='nav-bar'>
        <nav className='navbar'>
            <ul className='items'>
                <li className='item'>{moment().format('LLLL')}</li>
                <li className='item'>{props.user.username}</li>
            </ul>
        </nav>
    </div>);
}

export default NavBar;