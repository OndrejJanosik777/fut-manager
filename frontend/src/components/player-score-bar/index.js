import React, { Component } from 'react';
import "./index.scss";

const PlayerScoreBar = (props) => {
    return (<div className='player-score-bar'>
        <div className='name'>{props.name}</div>
        <div className='position'>{props.position}</div>
        <img className='image' src={props.imageUrl} alt="" />
        <div className='goals'>{props.goals}</div>
        <div className='assistences'>{props.assistances}</div>
        <div className='points'>{props.points}</div>
    </div>);
}

export default PlayerScoreBar;