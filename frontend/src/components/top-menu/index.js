import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import "../../pages/index.scss";
import "./index.scss";

const TopMenu = () => {
    const axios = require('axios');
    const navigate = useNavigate();

    const getBaseUrl = () => {
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // dev code
            return 'http://127.0.0.1:8000';
        } else {
            // production code
            return 'https://fut-v2.herokuapp.com';
        }
    }

    const [pageloaded, setPageLoaded] = useState(0);
    const [loggedUser, setLoggedUser] = useState({ username: '' });
    const [baseUrl, setBaseUrl] = useState(getBaseUrl());
    const [showLogInFields, setShowLogInFields] = useState(false);
    const [username, setUserName] = useState('');
    const [password, setPassword] = useState('');

    const loggOut = () => {
        localStorage.removeItem('FUT_user_token');
        setLoggedUser({ username: '' });
        alert('Loggout succesfully');

        navigate('/');
    }

    const login = () => {
        // console.log('logging in as: ');
        // console.log('username: ', username);
        // console.log('password: ', password);

        axios({
            method: 'post',
            url: baseUrl + '/api/token/',
            data: {
                username: username,
                password: password
            }
        })
            .then((response => {
                // localStorage.setItem('FUT_user_token', response.data.access);
                // console.log('saving token to storage ');
                setShowLogInFields(false);
                localStorage.setItem('FUT_user_token', response.data.access);
                alert('logged in succesfully');
                checkToken();
            }))
            .catch((error) => {
                console.log(error);
                alert('failed to log in');
            })
    }

    const renderTopMenu = () => {
        if (loggedUser.username !== '') {
            return <div className='TopMenu'>
                <div className='first-row'>
                    <span>Hello {loggedUser.username}</span>
                    <span className='isClickable' onClick={() => loggOut()} >Loggout</span>
                </div>
                <div className='second-row'>
                    <div><Link to="/" className='isClickable' >LANDING PAGE</Link></div>
                    <Link to="/database/" className='isClickable'>PLAYER'S DATABASE</Link>
                    <div><Link to={"/my-club/"} className='isClickable' >FUT CLUB</Link></div>
                    <div><Link to="/fut/add-goals" className='isClickable' >FUT - ADD SCORE</Link></div>
                    <div><Link to="/team/manage-your-team" className='isClickable' >SEASONS</Link></div>
                </div>
            </div>
        }
        else if (showLogInFields === true) {
            return <div className='TopMenu'>
                <div className='first-row'>
                    <div>Username: </div>
                    <input type='text' value={username} onChange={(e) => setUserName(e.target.value)} className='isInput' />
                    <div>Password: </div>
                    <input type='password' value={password} onChange={(e) => setPassword(e.target.value)} className='isInput' />
                    <input type='button' value='LOG IN' onClick={() => login()} className='isClickable' />
                </div>
            </div>
        }
        else {
            return <div className='TopMenu'>

                <div className='first-row'>Hello Guest, please <input type='button' value='LOGIN' className='isClickable' onClick={() => setShowLogInFields(true)} /> or <Link to="/sign-in/" className='isClickable' >SIGN-IN</Link></div>
                <div className='second-row'>
                    <Link to="/database/" className='isClickable'>PLAYER'S DATABASE</Link>
                </div>
            </div>
        }
    }

    const checkToken = () => {
        // console.log('COMPONENT: TOP-MENU: function checkToken...');

        const token = 'Bearer ' + localStorage.getItem('FUT_user_token');

        axios({
            method: 'get',
            url: baseUrl + '/api/fut/user/',
            headers: { 'Authorization': token },
        }).then((response) => {
            // console.log("loggedUser: ", response.data);

            // TODO: refresh token 

            setLoggedUser(response.data);
        }).catch((error) => {
            console.log(error);
        })
    }

    useEffect(() => {
        // console.log('COMPONENT: TOP-MENU: Mounting');

        checkToken();

    }, [pageloaded]);

    return (<div>
        <div>{renderTopMenu()}</div>
    </div>);
}

export default TopMenu;