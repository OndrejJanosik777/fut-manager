import React, { Component } from 'react'

const Button01 = (props) => {
    const componentStyle = {
        width: "100%",
        display: "flex",
        justifyContent: "space-arround"
    }

    const buttonStyle = {
        backgroundColor: "azure",
        padding: "0.2rem 0rem",
        width: "80%",
        margin: "0.25rem 0",
    };

    return ( <div style={componentStyle}>
        <input type='button' value={props.label} style={buttonStyle} onClick={props.action} />
    </div> );
}
 
export default Button01;