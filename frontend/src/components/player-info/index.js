import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
// import "../../pages/index.scss";
import "./index.scss";
import downArrow from './down-arrow.svg';

const PlayerInfo = (props) => {
    const axios = require('axios');

    const getBaseUrl = () => {
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // dev code
            return 'http://127.0.0.1:8000';
        } else {
            // production code
            return 'https://fut-v3.herokuapp.com';
        }
    }

    const [baseUrl, setBaseUrl] = useState(getBaseUrl());
    const [showExtendedAttribues, setShowExtendedAttribues] = useState(false);
    const [rarities, setRarities] = useState([]);

    const addPlayerToTeam = (id) => {
    }

    useEffect(() => {
        
    }, []);

    return (<div className={ props.isActive ? 'player-info player-info-active' : 'player-info'} onClick={() => props.modifyPlayerInTeam(props.player)} >
        <img className='avatar' src={baseUrl + '/api/fifa/get-players-image/' + props.player.id} alt='av.'  ></img>
        <div className='base-props'>
            <div className='commonName'>{props.player.commonName}</div>
            <div className='rating'>{props.player.rating} {props.player.position}</div>
        </div>
        <div className='personal'>
            <img className='img-nation' src={baseUrl + '/api/fifa/get-nations-image/' + props.player.nation} alt='av.'></img>
            <img className='img-league' src={baseUrl + '/api/fifa/get-leagues-image/' + props.player.league} alt='av.'></img>
            <img className='img-club' src={baseUrl + '/api/fifa/get-clubs-image/' + props.player.club} alt='av.'></img>
        </div>
    </div>);
}

export default PlayerInfo;