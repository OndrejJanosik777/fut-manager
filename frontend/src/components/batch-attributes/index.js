import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import goldMedal from "./assets/gold_medal.svg";
import silverMedal from "./assets/silver_medal.svg";
import bronzeMedal from "./assets/bronze_medal.svg";
import "./index.scss";

const BatchAttributes = (props) => {
    const [pageloaded, setPageLoaded] = useState(0);

    const showMedal = () => {
        if (props.batch === "gold") {
            return <img src={goldMedal} alt="gold medal" />;
        }
        if (props.batch === "silver") {
            return <img src={silverMedal} alt="silver medal" />;
        }
        if (props.batch === "bronze") {
            return <img src={bronzeMedal} alt="bronze medal" />;
        }
    }

    useEffect(() => {
        // console.log('COMPONENT: BATCH-ATTRIBUTES: pageloaded is updated...');

    }, [pageloaded]);

    return (<div className='batch-attributes'>
        <div className='type'>{props.type}</div>
        <div className='rank'>
            <div className='batch'>{showMedal()}</div>
            <div className='value'>{props.rank}</div>
        </div>
        <div className='name'>
            <div>{props.firstName}</div>
            <div>{props.lastName}</div>
        </div>
        <img
            className={props.scheme === "dark" ? "picture-dark" : "picture-transparent"}
            src={props.imageUrl} alt=''></img>
    </div>);
}

export default BatchAttributes;