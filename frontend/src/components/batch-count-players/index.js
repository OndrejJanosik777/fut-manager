import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import "./index.scss";

const BatchCountPlayers = (props) => {
    const [pageloaded, setPageLoaded] = useState(0);


    useEffect(() => {
        // console.log('COMPONENT: BATCH-COUNT-PLAYERS: Mounting Component');

    }, [pageloaded]);

    return (<div className='batch-count-players'>
        <div className='amount'>
            <strong className='number'>{props.amount}</strong>
        </div>
        <div>
            <img
                className={props.scheme === "dark" ? "picture-dark" : "picture-transparent"}
                src={props.imageUrl} alt=''></img>
        </div>
    </div>);
}

export default BatchCountPlayers;