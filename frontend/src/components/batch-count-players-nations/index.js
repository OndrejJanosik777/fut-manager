import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import "./index.scss";

const BatchCountPlayersNations = (props) => {

    const [pageloaded, setPageLoaded] = useState(0);


    useEffect(() => {
        // console.log('COMPONENT: BATCH-COUNT-PLAYERS-NATIONS: Mounting');

        // checkToken();

        // getMyPlayers();

    }, [pageloaded]);

    return (<div className='batch-count-players-nations'>
        <div className='amount'>
            <strong className='number'>{props.amount}</strong>
        </div>
        <div>
            <img className='picture' src={props.imageUrl} alt=''></img>
        </div>
    </div>);
}

export default BatchCountPlayersNations;